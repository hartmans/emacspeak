;;;Auto generated

;;;### (autoloads nil "g-load-path" "g-load-path.el" (0 0 0 0))
;;; Generated autoloads from g-load-path.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "g-load-path" '("g-directory")))

;;;***

;;;### (autoloads nil "g-utils" "g-utils.el" (0 0 0 0))
;;; Generated autoloads from g-utils.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "g-utils" '("g-")))

;;;***

;;;### (autoloads nil "gm-nnir" "gm-nnir.el" (0 0 0 0))
;;; Generated autoloads from gm-nnir.el

(autoload 'gm-nnir-group-make-nnir-group "gm-nnir" "\
GMail equivalent of gnus-group-make-nnir-group.
This uses standard IMap search operators.

\(fn Q)" t nil)

(autoload 'gm-nnir-group-make-gmail-group "gm-nnir" "\
Use GMail search syntax exclusively.
See https://support.google.com/mail/answer/7190?hl=en for syntax.
 note: nnimap-address etc are available as local vars if needed in these functions.

\(fn QUERY)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "gm-nnir" '("gm-nnir-")))

;;;***

;;;### (autoloads nil "gmaps" "gmaps.el" (0 0 0 0))
;;; Generated autoloads from gmaps.el

(autoload 'gmaps-address-location "gmaps" "\
Returns gmaps--location structure. Memoized to save network calls.

\(fn ADDRESS)" nil nil)

(autoload 'gmaps-address-geocode "gmaps" "\
Return lat/long for a given address.

\(fn ADDRESS)" nil nil)

(autoload 'gmaps-geocode "gmaps" "\
Geocode given address.
Optional argument `raw-p' returns complete JSON  object.

\(fn ADDRESS &optional RAW-P)" nil nil)

(autoload 'gmaps-reverse-geocode "gmaps" "\
Reverse geocode lat-long.
Optional argument `raw-p' returns raw JSON  object.

\(fn LAT-LONG &optional RAW-P)" nil nil)

(defvar gweb-my-address nil "\
Location address. Setting this updates gweb-my-location coordinates  via geocoding.")

(custom-autoload 'gweb-my-address "gmaps" nil)

(autoload 'gmaps "gmaps" "\
Google Maps Interaction.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "gmaps" '("gmaps-" "gweb-my-")))

;;;***

;;;### (autoloads nil "gskeleton" "gskeleton.el" (0 0 0 0))
;;; Generated autoloads from gskeleton.el

(autoload 'gskeleton-sign-out "gskeleton" "\
Resets client so you can start with a different userid.

\(fn)" t nil)

(autoload 'gskeleton-sign-in "gskeleton" "\
Resets client so you can start with a different userid.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "gskeleton" '("gskeleton-" "make-gskeleton-auth")))

;;;***

;;;### (autoloads nil "gweb" "gweb.el" (0 0 0 0))
;;; Generated autoloads from gweb.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "gweb" '("gweb-")))

;;;***

;;;### (autoloads nil nil ("g-cus-load.el" "g.el") (0 0 0 0))

;;;***

;;;### (autoloads nil "g-autogen" "g-autogen.el" (0 0 0 0))
;;; Generated autoloads from g-autogen.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "g-autogen" '("g-")))

;;;***
