;;; cus-load.el --- automatically extracted custom dependencies
;;
;;; Code:

(put 'accessibility 'custom-loads '(emacspeak))
(put 'amixer 'custom-loads '(amixer))
(put 'applications 'custom-loads '(emacspeak soundscape sox xbacklight))
(put 'cd-tool 'custom-loads '(cd-tool))
(put 'dtk 'custom-loads '(dtk-speak))
(put 'dtk-unicode 'custom-loads '(dtk-unicode))
(put 'emacspeak 'custom-loads '(dtk-speak dtk-unicode emacspeak emacspeak-add-log emacspeak-advice emacspeak-aumix emacspeak-bookshare emacspeak-dismal emacspeak-epub emacspeak-erc emacspeak-eshell emacspeak-eterm emacspeak-eudc emacspeak-feeds emacspeak-flyspell emacspeak-forms emacspeak-gnus emacspeak-ido emacspeak-ispell emacspeak-jss emacspeak-keymap emacspeak-kite emacspeak-m-player emacspeak-maths emacspeak-message emacspeak-midge emacspeak-npr emacspeak-nxml emacspeak-ocr emacspeak-pronounce emacspeak-setup emacspeak-sounds emacspeak-speak emacspeak-speedbar emacspeak-vlc emacspeak-vm emacspeak-we emacspeak-websearch emacspeak-widget emacspeak-wizards emacspeak-xml-shell emacspeak-xslt sox voice-setup xbacklight))
(put 'emacspeak-advice 'custom-loads '(emacspeak-advice))
(put 'emacspeak-aumix 'custom-loads '(emacspeak-aumix))
(put 'emacspeak-bbc 'custom-loads '(emacspeak-bbc))
(put 'emacspeak-bookshare 'custom-loads '(emacspeak-bookshare))
(put 'emacspeak-calendar 'custom-loads '(emacspeak-calendar))
(put 'emacspeak-dired 'custom-loads '(emacspeak-dired))
(put 'emacspeak-dismal 'custom-loads '(emacspeak-dismal))
(put 'emacspeak-eperiodic 'custom-loads '(emacspeak-eperiodic))
(put 'emacspeak-epub 'custom-loads '(emacspeak-epub))
(put 'emacspeak-erc 'custom-loads '(emacspeak-erc))
(put 'emacspeak-eshell 'custom-loads '(emacspeak-eshell))
(put 'emacspeak-eterm 'custom-loads '(emacspeak-eterm))
(put 'emacspeak-eudc 'custom-loads '(emacspeak-eudc))
(put 'emacspeak-eww 'custom-loads '(emacspeak-eww))
(put 'emacspeak-feeds 'custom-loads '(emacspeak-feeds))
(put 'emacspeak-flyspell 'custom-loads '(emacspeak-flyspell))
(put 'emacspeak-forms 'custom-loads '(emacspeak-forms))
(put 'emacspeak-gnus 'custom-loads '(emacspeak-gnus))
(put 'emacspeak-google 'custom-loads '(emacspeak-google))
(put 'emacspeak-hide 'custom-loads '(emacspeak-hide))
(put 'emacspeak-ido 'custom-loads '(emacspeak-ido))
(put 'emacspeak-info 'custom-loads '(emacspeak-info))
(put 'emacspeak-ispell 'custom-loads '(emacspeak-ispell))
(put 'emacspeak-jabber 'custom-loads '(emacspeak-jabber))
(put 'emacspeak-librivox 'custom-loads '(emacspeak-librivox))
(put 'emacspeak-m-player 'custom-loads '(emacspeak-m-player))
(put 'emacspeak-maths 'custom-loads '(emacspeak-maths))
(put 'emacspeak-media 'custom-loads '(emacspeak-m-player))
(put 'emacspeak-message 'custom-loads '(emacspeak-message))
(put 'emacspeak-midge 'custom-loads '(emacspeak-midge))
(put 'emacspeak-npr 'custom-loads '(emacspeak-npr))
(put 'emacspeak-ocr 'custom-loads '(emacspeak-ocr))
(put 'emacspeak-org 'custom-loads '(emacspeak-org))
(put 'emacspeak-outline 'custom-loads '(emacspeak-outline))
(put 'emacspeak-pianobar 'custom-loads '(emacspeak-pianobar))
(put 'emacspeak-pronounce 'custom-loads '(emacspeak-pronounce))
(put 'emacspeak-remote 'custom-loads '(emacspeak-remote))
(put 'emacspeak-sounds 'custom-loads '(emacspeak-sounds))
(put 'emacspeak-speak 'custom-loads '(emacspeak-advice emacspeak-speak emacspeak-wizards))
(put 'emacspeak-speedbar 'custom-loads '(emacspeak-speedbar))
(put 'emacspeak-vlc 'custom-loads '(emacspeak-vlc))
(put 'emacspeak-vm 'custom-loads '(emacspeak-vm))
(put 'emacspeak-we 'custom-loads '(emacspeak-we))
(put 'emacspeak-websearch 'custom-loads '(emacspeak-websearch))
(put 'emacspeak-webspace 'custom-loads '(emacspeak-webspace))
(put 'emacspeak-webutils 'custom-loads '(emacspeak-webutils))
(put 'emacspeak-wizards 'custom-loads '(emacspeak-wizards))
(put 'emacspeak-xml-shell 'custom-loads '(emacspeak-xml-shell))
(put 'emacspeak-xsl 'custom-loads '(emacspeak-feeds))
(put 'emacspeak-xslt 'custom-loads '(emacspeak-xslt))
(put 'eshell 'custom-loads '(emacspeak-eshell))
(put 'eudc 'custom-loads '(emacspeak-eudc))
(put 'flyspell 'custom-loads '(emacspeak-flyspell))
(put 'forms 'custom-loads '(emacspeak-forms))
(put 'games 'custom-loads '(tetris))
(put 'gnus 'custom-loads '(emacspeak-gnus))
(put 'isearch 'custom-loads '(emacspeak-advice))
(put 'message 'custom-loads '(emacspeak-message))
(put 'soundscape 'custom-loads '(soundscape))
(put 'sox 'custom-loads '(sox sox-gen))
(put 'sox-gen 'custom-loads '(sox-gen))
(put 'speedbar 'custom-loads '(emacspeak-speedbar))
(put 'tetris 'custom-loads '(tetris))
(put 'tts 'custom-loads '(dectalk-voices dtk-speak emacspeak-setup espeak-voices mac-voices outloud-voices))
(put 'vm 'custom-loads '(emacspeak-vm))
(put 'voice-fonts 'custom-loads '(voice-setup))
(put 'voice-lock 'custom-loads '(voice-setup))
(put 'widget 'custom-loads '(emacspeak-widget))
(put 'xbacklight 'custom-loads '(xbacklight))

;; The remainder of this file is for handling :version.
;; We provide a minimum of information so that `customize-changed-options'
;; can do its job.

;; For groups we set `custom-version', `group-documentation' and
;; `custom-tag' (which are shown in the customize buffer), so we
;; don't have to load the file containing the group.

;; This macro is used so we don't modify the information about
;; variables and groups if it's already set. (We don't know when
;; cus-load.el is going to be loaded and at that time some of the
;; files might be loaded and some others might not).
(defmacro custom-put-if-not (symbol propname value)
  `(unless (get ,symbol ,propname)
     (put ,symbol ,propname ,value)))


(defvar custom-versions-load-alist '(("37.0" emacspeak-advice) ("47" emacspeak-bookshare))
  "For internal use by custom.
This is an alist whose members have as car a version string, and as
elements the files that have variables or faces that contain that
version.  These files should be loaded before showing the customization
buffer that `customize-changed-options' generates.")


(provide 'cus-load)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; cus-load.el ends here
