;;;Auto generated

;;;### (autoloads nil "amixer" "amixer.el" (0 0 0 0))
;;; Generated autoloads from amixer.el

(defvar amixer-alsactl-config-file nil "\
Personal sound card settings. Copied from /var/lib/alsa/asound.state
to your ~/.emacs.d to avoid needing to run alsactl as root on first
use.")

(autoload 'amixer-restore "amixer" "\
Restore alsa settings.

\(fn &optional CONF-FILE)" nil nil)

(autoload 'amixer "amixer" "\
Interactively manipulate ALSA settings.
Interactive prefix arg refreshes cache.

\(fn &optional REFRESH)" t nil)

(autoload 'amixer-equalize "amixer" "\
Set equalizer. Only affects device `equal'.

\(fn)" t nil)

(autoload 'amixer-store "amixer" "\
Persist current amixer settings.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "amixer" '("alsactl-program" "amixer-")))

;;;***

;;;### (autoloads nil "cd-tool" "cd-tool.el" (0 0 0 0))
;;; Generated autoloads from cd-tool.el

(autoload 'cd-tool "cd-tool" "\
Front-end to CDTool.
Bind this function to a convenient key-
Emacspeak users automatically have
this bound to <DEL> in the emacspeak keymap.

Key     Action
---     ------

+       Next Track
-       Previous Track
SPC     Pause or Resume
e       Eject
=       Shuffle
i       CD Info
p       Play
s       Stop
t       track
c       clip
cap C   Save clip to disk

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "cd-tool" '("cd-tool-")))

;;;***

;;;### (autoloads nil "dectalk-voices" "dectalk-voices.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from dectalk-voices.el

(defvar dectalk-default-speech-rate 225 "\
*Default speech rate at which TTS is started. ")

(custom-autoload 'dectalk-default-speech-rate "dectalk-voices" nil)

(autoload 'dectalk-configure-tts "dectalk-voices" "\
Configures TTS environment to use Dectalk family of synthesizers.

\(fn)" nil nil)

(autoload 'dectalk-make-tts-env "dectalk-voices" "\
Constructs a TTS environment for Dectalk.

\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dectalk-voices" '("dectalk" "dtk-exp")))

;;;***

;;;### (autoloads nil "dom-addons" "dom-addons.el" (0 0 0 0))
;;; Generated autoloads from dom-addons.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dom-addons" '("dom-")))

;;;***

;;;### (autoloads nil "dtk-interp" "dtk-interp.el" (0 0 0 0))
;;; Generated autoloads from dtk-interp.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dtk-interp" '("dtk-interp-" "tts-with-")))

;;;***

;;;### (autoloads nil "dtk-speak" "dtk-speak.el" (0 0 0 0))
;;; Generated autoloads from dtk-speak.el

(defvar dtk-program (cond ((getenv "DTK_PROGRAM") (getenv "DTK_PROGRAM")) ((eq system-type 'darwin) "mac") (t "espeak")) "\
The program to use to talk to the speech engine.
Possible choices at present:
dtk-exp     For the Dectalk Express.
dtk-mv      for the Multivoice and older Dectalks.
outloud     For IBM ViaVoice Outloud
espeak      For eSpeak (default on Linux)
mac for MAC TTS (default on Mac)")

(defvar tts-strip-octals nil "\
Set to T to strip all octal chars before speaking.
Particularly useful for web browsing.")

(custom-autoload 'tts-strip-octals "dtk-speak" t)

(defvar dtk-speech-rate-base (if (string-match "dtk" dtk-program) 180 50) "\
*Value of lowest tolerable speech rate.")

(custom-autoload 'dtk-speech-rate-base "dtk-speak" t)

(defvar dtk-speech-rate-step (if (string-match "dtk" dtk-program) 50 8) "\
*Value of speech rate increment.
This determines step size used when setting speech rate via command
`dtk-set-predefined-speech-rate'.  Formula used is
dtk-speech-rate-base  +  dtk-speech-rate-step*level.")

(custom-autoload 'dtk-speech-rate-step "dtk-speak" t)

(defvar dtk-cleanup-patterns (list "." "_" "-" "=" "/" "+" "*" ":" ";" "%" "\\/" "/\\" "{" "}" "~" "$" ")" "#" "<>") "\
List of repeating patterns to clean up.
You can use  command  `dtk-add-cleanup-pattern'
 bound to \\[dtk-add-cleanup-pattern]  to add more patterns.
Specify patterns that people use to decorate their ASCII files, and cause
untold pain to the speech synthesizer.

If more than 3 consecutive occurrences
of a specified pattern is found, the TTS engine replaces it
with a repeat count. ")

(custom-autoload 'dtk-cleanup-patterns "dtk-speak" t)

(autoload 'dtk-get-voice-for-face "dtk-speak" "\
Compute face->voice mapping.

\(fn VALUE)" nil nil)

(defvar dtk-use-tones t "\
Allow tones to be turned off.")

(custom-autoload 'dtk-use-tones "dtk-speak" t)

(defvar dtk-speak-nonprinting-chars nil "\
*Option that specifies handling of non-printing chars.
Non nil value means non printing characters  should be
spoken as their octal value.
Set this to t to avoid a dectalk bug that makes the speech box die if
it seems some accented characters in certain contexts.")

(custom-autoload 'dtk-speak-nonprinting-chars "dtk-speak" t)

(autoload 'dtk-stop "dtk-speak" "\
Stop speech now.
Optional arg `all' or interactive call   silences notification stream as well.

\(fn &optional ALL)" t nil)

(autoload 'dtk-add-cleanup-pattern "dtk-speak" "\
Add this pattern to the list of repeating patterns that
are cleaned up.  Optional interactive prefix arg deletes
this pattern if previously added.  Cleaning up repeated
patterns results in emacspeak speaking the pattern followed
by a repeat count instead of speaking all the characters
making up the pattern.  Thus, by adding the repeating
pattern `.' (this is already added by default) emacspeak
will say ``aw fifteen dot'' when speaking the string
``...............'' instead of ``period period period period
''

\(fn &optional DELETE)" t nil)

(autoload 'ems-generate-switcher "dtk-speak" "\
Generate desired command to switch the specified state.

\(fn COMMAND SWITCH DOCUMENTATION)" nil nil)

(autoload 'dtk-set-rate "dtk-speak" "\
Set speaking RATE for the tts.
Interactive PREFIX arg means set   the global default value, and then set the
current local  value to the result.

\(fn RATE &optional PREFIX)" t nil)

(autoload 'dtk-set-predefined-speech-rate "dtk-speak" "\
Set speech rate to one of nine predefined levels.
Interactive PREFIX arg says to set the rate globally.
Formula used is:
rate = dtk-speech-rate-base + dtk-speech-rate-step * level.

\(fn &optional PREFIX)" t nil)

(autoload 'dtk-set-character-scale "dtk-speak" "\
Set scale FACTOR for   speech rate.
Speech rate is scaled by this factor
when speaking characters.
Interactive PREFIX arg means set   the global default value, and then set the
current local  value to the result.

\(fn FACTOR &optional PREFIX)" t nil)

(autoload 'dtk-set-punctuations "dtk-speak" "\
Set punctuation mode to MODE.
Possible values are `some', `all', or `none'.
Interactive PREFIX arg means set   the global default value, and then set the
current local  value to the result.

\(fn MODE &optional PREFIX)" t nil)

(autoload 'dtk-set-punctuations-to-all "dtk-speak" "\
Set punctuation  mode to all.
Interactive PREFIX arg sets punctuation mode globally.

\(fn &optional PREFIX)" t nil)

(autoload 'dtk-set-punctuations-to-some "dtk-speak" "\
Set punctuation  mode to some.
Interactive PREFIX arg sets punctuation mode globally.

\(fn &optional PREFIX)" t nil)

(autoload 'dtk-toggle-punctuation-mode "dtk-speak" "\
Toggle punctuation mode between \"some\" and \"all\".
Interactive PREFIX arg makes the new setting global.

\(fn &optional PREFIX)" t nil)

(autoload 'dtk-reset-state "dtk-speak" "\
Restore sanity to the Dectalk.
Typically used after the Dectalk has been power   cycled.

\(fn)" t nil)

(autoload 'tts-speak-version "dtk-speak" "\
Speak version.

\(fn)" t nil)

(autoload 'tts-configure-synthesis-setup "dtk-speak" "\
Setup synthesis environment. 

\(fn &optional TTS-NAME)" nil nil)

(defvar dtk-cloud-server "cloud-outloud" "\
Set this to your preferred cloud TTS server.")

(custom-autoload 'dtk-cloud-server "dtk-speak" t)

(autoload 'dtk-select-server "dtk-speak" "\
Select a speech server interactively.
When called interactively, restarts speech server.
Argument PROGRAM specifies the speech server program.
 Optional arg device sets up environment variable
ALSA_DEFAULT to specified device before starting the server.

\(fn PROGRAM &optional DEVICE)" t nil)

(autoload 'dtk-cloud "dtk-speak" "\
Select preferred Cloud TTS server.

\(fn)" t nil)

(autoload 'tts-cycle-device "dtk-speak" "\
Cycle through available ALSA devices.
Optional interactive prefix arg restarts current TTS server.

\(fn &optional RESTART)" t nil)

(defvar dtk-local-server-process nil "\
Local server process.")

(defvar dtk-speech-server-program "speech-server" "\
Local speech server script.")

(custom-autoload 'dtk-speech-server-program "dtk-speak" t)

(defvar dtk-local-engine "outloud" "\
Engine we use  for our local TTS  server.")

(custom-autoload 'dtk-local-engine "dtk-speak" t)

(autoload 'tts-shutdown "dtk-speak" "\
Shutdown TTS servers.

\(fn)" nil nil)

(autoload 'tts-restart "dtk-speak" "\
Use this to nuke the currently running TTS server and restart it.

\(fn)" t nil)

(autoload 'dtk-toggle-splitting-on-white-space "dtk-speak" "\
Toggle splitting of speech on white space.
This affects the internal state of emacspeak that decides if we split
text purely by clause boundaries, or also include
whitespace.  By default, emacspeak sends a clause at a time
to the speech device.  This produces fluent speech for
normal use.  However in modes such as `shell-mode' and some
programming language modes, clause markers appear
infrequently, and this can result in large amounts of text
being sent to the speech device at once, making the system
unresponsive when asked to stop talking.  Splitting on white
space makes emacspeak's stop command responsive.  However,
when splitting on white space, the speech sounds choppy
since the synthesizer is getting a word at a time.

\(fn)" t nil)

(autoload 'dtk-set-chunk-separator-syntax "dtk-speak" "\
Interactively set how text is split in chunks.
See the Emacs documentation on syntax tables for details on how characters are
classified into various syntactic classes.
Argument S specifies the syntax class.

\(fn S)" t nil)

(autoload 'dtk-notify-stop "dtk-speak" "\
Stop  speech on notification stream.

\(fn)" t nil)

(autoload 'dtk-notify-speak "dtk-speak" "\
Speak text on notification stream.
Fall back to dtk-speak if notification stream not available.

\(fn TEXT &optional DONT-LOG)" nil nil)

(autoload 'dtk-notify-say "dtk-speak" "\
Say text on notification stream. 

\(fn TEXT &optional DONT-LOG)" nil nil)

(autoload 'dtk-notify-letter "dtk-speak" "\
Speak letter on notification stream. 

\(fn LETTER)" nil nil)

(autoload 'dtk-notify-initialize "dtk-speak" "\
Initialize notification TTS stream.

\(fn)" t nil)

(autoload 'dtk-notify-using-voice "dtk-speak" "\
Use voice VOICE to speak text TEXT on notification stream.

\(fn VOICE TEXT &optional DONT-LOG)" nil nil)

(autoload 'dtk-notify-shutdown "dtk-speak" "\
Shutdown notification TTS stream.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dtk-speak" '("delete-invisible-text" "dtk-" "emacspeak-servers-directory" "ems-with-messages-silenced" "skip-invisible-" "text-visible-p" "tts-" "voice-lock-mode")))

;;;***

;;;### (autoloads nil "dtk-unicode" "dtk-unicode.el" (0 0 0 0))
;;; Generated autoloads from dtk-unicode.el

(defvar dtk-unicode-character-replacement-alist '((150 . "-") (9473 . "-") (9475 . "|") (176 . " degrees ") (8451 . "Degree C") (8457 . "Degree F ") (8220 . "\"") (8221 . "\"") (8902 . "*") (173 . "-") (8216 . " backquote  ") (8217 . "'") (8208 . "hyphen") (8211 . " -- ") (8212 . " --- ") (8202 . " ") (8213 . "----") (8214 . "||") (8230 . "...") (8226 . " bullet ") (149 . " ... ") (8482 . "TM") (64256 . "ff") (64257 . "fi") (64258 . "fl") (64259 . "ffi") (64260 . "Ffl")) "\
Explicit replacements for some characters.")

(custom-autoload 'dtk-unicode-character-replacement-alist "dtk-unicode" t)

(defvar dtk-unicode-process-utf8 t "\
Turn this off when working with TTS  engines that handle UTF8
themselves, e.g., when using an Asian language.")

(custom-autoload 'dtk-unicode-process-utf8 "dtk-unicode" t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "dtk-unicode" '("dtk-unicode-")))

;;;***

;;;### (autoloads nil "emacspeak" "emacspeak.el" (0 0 0 0))
;;; Generated autoloads from emacspeak.el

(defvar emacspeak-media-player 'emacspeak-m-player "\
Default media player to use.
This is a Lisp function that takes a resource locator.")

(autoload 'emacspeak-setup-programming-mode "emacspeak" "\
Setup programming mode.
Turns on audio indentation and sets
punctuation mode to all, activates the dictionary and turns on split
caps.

\(fn)" nil nil)

(defvar emacspeak-play-emacspeak-startup-icon t "\
If set to T, emacspeak plays its icon as it launches.")

(custom-autoload 'emacspeak-play-emacspeak-startup-icon "emacspeak" t)

(autoload 'emacspeak "emacspeak" "\
Start the Emacspeak Audio Desktop.
Use Emacs as you normally would,
emacspeak will provide you spoken feedback as you work.  Emacspeak also
provides commands for having parts of the current buffer, the
mode-line etc to be spoken.

If you are hearing this description as a result of pressing
\\[emacspeak-describe-emacspeak] you may want to press \\[dtk-stop] to
stop speech, and then use the arrow keys to move around in the Help
buffer to read the rest of this description, which includes a summary
of all emacspeak keybindings.

All emacspeak commands use \\[emacspeak-prefix-command] as a prefix
key.  You can also set the state of the TTS engine by using
\\[emacspeak-dtk-submap-command] as a prefix.  Here is a summary of all
emacspeak commands along with their bindings.  You need to precede the
keystrokes listed below with \\[emacspeak-prefix-command].

Emacspeak also provides a fluent speech extension to the Emacs
terminal emulator (eterm).  Note: You need to use the term package that
comes with emacs-19.29 and later.

\\{emacspeak-keymap}

Emacspeak provides a set of additional keymaps to give easy access to
its extensive facilities.

Press C-; to access keybindings in emacspeak-hyper-keymap:
\\{emacspeak-hyper-keymap}.

Press C-' or C-.  to access keybindings in emacspeak-super-keymap:
\\{emacspeak-super-keymap}.

Press C-, to access keybindings in emacspeak-alt-keymap:
\\{emacspeak-alt-keymap}.

See the online documentation \\[emacspeak-open-info] for individual
commands and options for details.

\(fn)" t nil)

(autoload 'emacspeak-info "emacspeak" "\
Open Emacspeak Info Manual.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak" '("emacspeak-")))

;;;***

;;;### (autoloads nil "emacspeak-2048" "emacspeak-2048.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-2048.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-2048" '("emacspeak-2048-")))

;;;***

;;;### (autoloads nil "emacspeak-actions" "emacspeak-actions.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-actions.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-actions" '("emacspeak-")))

;;;***

;;;### (autoloads nil "emacspeak-advice" "emacspeak-advice.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-advice.el

(defvar emacspeak-untabify-fixes-non-breaking-space t "\
Advice untabify to change non-breaking space chars to space.")

(custom-autoload 'emacspeak-untabify-fixes-non-breaking-space "emacspeak-advice" t)

(defvar emacspeak-speak-errors t "\
Specifies if error messages are cued.")

(custom-autoload 'emacspeak-speak-errors "emacspeak-advice" t)

(defvar emacspeak-speak-signals t "\
Specifies if signalled messages are cued.")

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-advice" '("dirtrack-procfs-mode" "emacspeak-" "ems-canonicalize-key-description")))

;;;***

;;;### (autoloads nil "emacspeak-amark" "emacspeak-amark.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-amark.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-amark" '("emacspeak-amark-")))

;;;***

;;;### (autoloads nil "emacspeak-analog" "emacspeak-analog.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-analog.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-analog" '("emacspeak-analog-")))

;;;***

;;;### (autoloads nil "emacspeak-ansi-color" "emacspeak-ansi-color.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-ansi-color.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-ansi-color" '("emacspeak-ansi-color-to-voice")))

;;;***

;;;### (autoloads nil "emacspeak-arc" "emacspeak-arc.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-arc.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-arc" '("emacspeak-arc")))

;;;***

;;;### (autoloads nil "emacspeak-aumix" "emacspeak-aumix.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-aumix.el

(let ((loads (get 'emacspeak-aumix 'custom-loads))) (if (member '"emacspeak-aumix" loads) nil (put 'emacspeak-aumix 'custom-loads (cons '"emacspeak-aumix" loads))))

(defvar emacspeak-aumix-settings-file (when (file-exists-p (expand-file-name ".aumixrc" emacspeak-resource-directory)) (expand-file-name ".aumixrc" emacspeak-resource-directory)) "\
*Name of file containing personal aumix settings.")

(custom-autoload 'emacspeak-aumix-settings-file "emacspeak-aumix" t)

(defvar emacspeak-alsactl-program "alsactl" "\
ALSA sound controller used to restore settings.")

(custom-autoload 'emacspeak-alsactl-program "emacspeak-aumix" t)

(defvar emacspeak-aumix-reset-options (format "-f %s -L 2>&1 >/dev/null" emacspeak-aumix-settings-file) "\
*Option to pass to aumix for resetting to default values.")

(custom-autoload 'emacspeak-aumix-reset-options "emacspeak-aumix" t)

(autoload 'emacspeak-aumix-reset "emacspeak-aumix" "\
Reset to default audio settings.

\(fn)" t nil)

(autoload 'emacspeak-aumix "emacspeak-aumix" "\
Setup output parameters of the auditory display.
 Launch this tool while you have auditory output on
multiple channels playing so you can
adjust the settings to your preference.  Hit q to quit when
you are done.

\(fn)" t nil)

(autoload 'emacspeak-aumix-wave-increase "emacspeak-aumix" "\
Increase volume of wave output. 

\(fn &optional GAIN)" t nil)

(autoload 'emacspeak-aumix-wave-decrease "emacspeak-aumix" "\
Decrease volume of wave output. 

\(fn &optional GAIN)" t nil)

(autoload 'emacspeak-aumix-volume-increase "emacspeak-aumix" "\
Increase overall volume. 

\(fn &optional GAIN)" t nil)

(autoload 'emacspeak-aumix-volume-decrease "emacspeak-aumix" "\
Decrease overall volume. 

\(fn &optional GAIN)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-aumix" '("emacspeak-aumix-")))

;;;***

;;;### (autoloads nil "emacspeak-autoload" "emacspeak-autoload.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-autoload.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-autoload" '("emacspeak-")))

;;;***

;;;### (autoloads nil "emacspeak-bbc" "emacspeak-bbc.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-bbc.el

(autoload 'emacspeak-bbc-get-iplayer-stream-url "emacspeak-bbc" "\
Stream using get_iplayer.

\(fn URL)" t nil)

(autoload 'emacspeak-bbc-get-iplayer-stream-pid "emacspeak-bbc" "\
Stream using get_iplayer.

\(fn PID)" t nil)

(autoload 'emacspeak-bbc "emacspeak-bbc" "\
Launch BBC Interaction.
See http://www.bbc.co.uk/radio/stations for full list of stations.
See http://www.bbc.co.uk/radio/programmes/genres for list of genres.
Interactive prefix arg filters  content by genre.

\(fn &optional GENRE)" t nil)

(autoload 'emacspeak-bbc-genre "emacspeak-bbc" "\
Launch BBC Interaction for specified Genre.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-bbc" '("emacspeak-bbc-")))

;;;***

;;;### (autoloads nil "emacspeak-bookshare" "emacspeak-bookshare.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-bookshare.el

(defvar emacspeak-bookshare-browser-function 'eww-browse-url "\
Function to display Bookshare Book content in a WWW browser.
This is used by the various Bookshare view commands to display
  content from Bookshare books.")

(custom-autoload 'emacspeak-bookshare-browser-function "emacspeak-bookshare" t)

(autoload 'emacspeak-bookshare "emacspeak-bookshare" "\
Bookshare  Interaction.

\(fn)" t nil)

(defvar emacspeak-bookshare-xslt "daisyTransform.xsl" "\
Name of bookshare  XSL transform.")

(custom-autoload 'emacspeak-bookshare-xslt "emacspeak-bookshare" t)

(defvar emacspeak-bookshare-html-to-text-command "lynx -dump -stdin" "\
Command to convert html to text on stdin.")

(custom-autoload 'emacspeak-bookshare-html-to-text-command "emacspeak-bookshare" t)

(autoload 'emacspeak-bookshare-eww "emacspeak-bookshare" "\
Render complete book using EWW

\(fn DIRECTORY)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-bookshare" '("emacspeak-bookshare-")))

;;;***

;;;### (autoloads nil "emacspeak-bs" "emacspeak-bs.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-bs.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-bs" '("emacspeak-bs-speak-buffer-line")))

;;;***

;;;### (autoloads nil "emacspeak-buff-menu" "emacspeak-buff-menu.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-buff-menu.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-buff-menu" '("emacspeak-list-buffers-")))

;;;***

;;;### (autoloads nil "emacspeak-c" "emacspeak-c.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-c.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-c" '("c-" "emacspeak-c-s")))

;;;***

;;;### (autoloads nil "emacspeak-calculator" "emacspeak-calculator.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-calculator.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-calculator" '("emacspeak-calculator-summarize")))

;;;***

;;;### (autoloads nil "emacspeak-calendar" "emacspeak-calendar.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-calendar.el

(autoload 'emacspeak-appt-repeat-announcement "emacspeak-calendar" "\
Speaks the most recently displayed appointment message if any.

\(fn)" t nil)

(autoload 'emacspeak-calendar-setup-sunrise-sunset "emacspeak-calendar" "\
Set up geo-coordinates using Google Maps reverse geocoding.
To use, configure variable gweb-my-address via M-x customize-variable.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-calendar" '("emacspeak-")))

;;;***

;;;### (autoloads nil "emacspeak-company" "emacspeak-company.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-company.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-company" '("emacspeak-company-" "ems-company-current")))

;;;***

;;;### (autoloads nil "emacspeak-compile" "emacspeak-compile.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-compile.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-compile" '("emacspeak-compilation-speak-error")))

;;;***

;;;### (autoloads nil "emacspeak-cperl" "emacspeak-cperl.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-cperl.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-cperl" '("emacspeak-cperl-electric-insertion-commands-to-advice")))

;;;***

;;;### (autoloads nil "emacspeak-custom" "emacspeak-custom.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-custom.el

(autoload 'emacspeak-custom-goto-group "emacspeak-custom" "\
Jump to custom group when in a customization buffer.

\(fn)" t nil)

(autoload 'emacspeak-custom-goto-toolbar "emacspeak-custom" "\
Jump to custom toolbar when in a customization buffer.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-custom" '("emacspeak-custom-")))

;;;***

;;;### (autoloads nil "emacspeak-dbus" "emacspeak-dbus.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-dbus.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-dbus" '("emacspeak-")))

;;;***

;;;### (autoloads nil "emacspeak-dired" "emacspeak-dired.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-dired.el

(autoload 'emacspeak-locate-play-results-as-playlist "emacspeak-dired" "\
Treat locate results as a play-list.
Optional interactive prefix arg shuffles playlist.

\(fn &optional SHUFFLE)" t nil)

(autoload 'emacspeak-dired-downloads "emacspeak-dired" "\
Open Downloads directory.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-dired" '("emacspeak-dired-")))

;;;***

;;;### (autoloads nil "emacspeak-dismal" "emacspeak-dismal.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-dismal.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-dismal" '("emacspeak-dismal-")))

;;;***

;;;### (autoloads nil "emacspeak-ecb" "emacspeak-ecb.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ecb.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-ecb" '("emacspeak-ecb-" "tree-node-is-expanded")))

;;;***

;;;### (autoloads nil "emacspeak-ediary" "emacspeak-ediary.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-ediary.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-ediary" '("emacspeak-ediary-commands-that-speak-entry")))

;;;***

;;;### (autoloads nil "emacspeak-ediff" "emacspeak-ediff.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-ediff.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-ediff" '("emacspeak-ediff-")))

;;;***

;;;### (autoloads nil "emacspeak-ein" "emacspeak-ein.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ein.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-ein" '("emacspeak-ein-speak-current-cell")))

;;;***

;;;### (autoloads nil "emacspeak-elfeed" "emacspeak-elfeed.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-elfeed.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-elfeed" '("emacspeak-elfeed-")))

;;;***

;;;### (autoloads nil "emacspeak-emms" "emacspeak-emms.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-emms.el

(autoload 'emacspeak-emms-pause-or-resume "emacspeak-emms" "\
Pause/resume if emms is running. For use  in
emacspeak-silence-hook.

\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-emms" '("emacspeak-emms-speak-current-track")))

;;;***

;;;### (autoloads nil "emacspeak-enriched" "emacspeak-enriched.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-enriched.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-enriched" '("emacspeak-enriched-")))

;;;***

;;;### (autoloads nil "emacspeak-entertain" "emacspeak-entertain.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-entertain.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-entertain" '("emacspeak-hangman-s")))

;;;***

;;;### (autoloads nil "emacspeak-eperiodic" "emacspeak-eperiodic.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-eperiodic.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-eperiodic" '("emacspeak-eperiodic-")))

;;;***

;;;### (autoloads nil "emacspeak-epub" "emacspeak-epub.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-epub.el

(defvar emacspeak-epub-library-directory (expand-file-name "~/EBooks/") "\
Directory under which we store Epubs.")

(custom-autoload 'emacspeak-epub-library-directory "emacspeak-epub" t)

(defvar emacspeak-epub-html-to-text-command "lynx -dump -stdin" "\
Command to convert html to text on stdin.")

(custom-autoload 'emacspeak-epub-html-to-text-command "emacspeak-epub" t)

(autoload 'emacspeak-epub-bookshelf-save "emacspeak-epub" "\
Save bookshelf metadata.

\(fn)" t nil)

(autoload 'emacspeak-epub "emacspeak-epub" "\
EPub  Interaction.
For detailed documentation, see \\[emacspeak-epub-mode]

\(fn)" t nil)

(autoload 'emacspeak-epub-open "emacspeak-epub" "\
Open specified Epub.
Filename may need to  be shell-quoted when called from Lisp.

\(fn EPUB-FILE)" t nil)

(autoload 'emacspeak-epub-eww "emacspeak-epub" "\
Display entire book  using EWW from EPub.

\(fn EPUB-FILE)" t nil)

(autoload 'emacspeak-epub-google "emacspeak-epub" "\
Search for Epubs from Google Books.

\(fn QUERY)" t nil)

(autoload 'emacspeak-epub-bookshelf-refresh "emacspeak-epub" "\
Refresh and redraw bookshelf.

\(fn)" t nil)

(autoload 'emacspeak-epub-gutenberg-download "emacspeak-epub" "\
Open web page for specified book.
Place download url for epub in kill ring.
With interactive prefix arg `download', download the epub.

\(fn BOOK-ID &optional DOWNLOAD)" t nil)

(defvar emacspeak-epub-calibre-root-dir (expand-file-name "calibre" emacspeak-epub-library-directory) "\
Root of Calibre library.")

(custom-autoload 'emacspeak-epub-calibre-root-dir "emacspeak-epub" t)

(defvar emacspeak-epub-calibre-sqlite (executable-find "sqlite3") "\
Path to sqlite3.")

(custom-autoload 'emacspeak-epub-calibre-sqlite "emacspeak-epub" t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-epub" '("emacspeak-" "epub-toc-xsl")))

;;;***

;;;### (autoloads nil "emacspeak-erc" "emacspeak-erc.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-erc.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-erc" '("emacspeak-erc-")))

;;;***

;;;### (autoloads nil "emacspeak-eshell" "emacspeak-eshell.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-eshell.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-eshell" '("emacspeak-eshell-")))

;;;***

;;;### (autoloads nil "emacspeak-etable" "emacspeak-etable.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-etable.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-etable" '("emacspeak-etable-speak-cell")))

;;;***

;;;### (autoloads nil "emacspeak-eterm" "emacspeak-eterm.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-eterm.el

(let ((loads (get 'emacspeak-eterm 'custom-loads))) (if (member '"emacspeak-eterm" loads) nil (put 'emacspeak-eterm 'custom-loads (cons '"emacspeak-eterm" loads))))

(autoload 'emacspeak-eterm-record-window "emacspeak-eterm" "\
Insert this window definition into the table of terminal windows.
Argument WINDOW-ID specifies the window.
Argument TOP-LEFT  specifies top-left of window.
Argument BOTTOM-RIGHT  specifies bottom right of window.
Optional argument RIGHT-STRETCH  specifies if the window stretches to the right.
Optional argument LEFT-STRETCH  specifies if the window stretches to the left.

\(fn WINDOW-ID TOP-LEFT BOTTOM-RIGHT &optional RIGHT-STRETCH LEFT-STRETCH)" nil nil)

(autoload 'emacspeak-eterm-set-filter-window "emacspeak-eterm" "\
Prompt for the id of a predefined window,
and set the `filter' window to it.
Non-nil interactive prefix arg `unsets' the filter window;
this is equivalent to having the entire terminal as the filter window (this is
what eterm starts up with).
Setting the filter window results in emacspeak  only monitoring screen
activity within the filter window.

\(fn FLAG)" t nil)

(defvar emacspeak-eterm-remote-hosts-table (make-vector 127 0) "\
obarray used for completing hostnames when prompting for a remote
host. Hosts are added whenever a new hostname is encountered, and the
list of known hostnames is persisted in file named by
emacspeak-eterm-remote-hostnames")

(autoload 'emacspeak-eterm-cache-remote-host "emacspeak-eterm" "\
Add this hostname to cache of remote hostnames

\(fn HOST)" nil nil)

(autoload 'emacspeak-eterm-remote-term "emacspeak-eterm" "\
Start a terminal-emulator in a new buffer.

\(fn HOST)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-eterm" '("emacspeak-eterm-" "eterm-")))

;;;***

;;;### (autoloads nil "emacspeak-eudc" "emacspeak-eudc.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-eudc.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-eudc" '("emacspeak-eudc-")))

;;;***

;;;### (autoloads nil "emacspeak-evil" "emacspeak-evil.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-evil.el

(autoload 'emacspeak-evil-toggle-evil "emacspeak-evil" "\
Interactively toggle evil-mode.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-evil" '("emacspeak-evil-fix-emacspeak-prefix")))

;;;***

;;;### (autoloads nil "emacspeak-eww" "emacspeak-eww.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-eww.el

(autoload 'emacspeak-eww-open-mark "emacspeak-eww" "\
Open specified EWW marked location. If the content is already being
displayed in this Emacs session, jump to it directly. With optional
interactive prefix arg `delete', delete that mark instead.

\(fn NAME &optional DELETE)" t nil)

(autoload 'emacspeak-eww-shell-command-on-url-at-point "emacspeak-eww" "\
Run specified shell command on URL at point.
Warning: Running shell script cbox through this fails mysteriously.

\(fn &optional PREFIX)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-eww" '("emacspeak-eww-" "eww-")))

;;;***

;;;### (autoloads nil "emacspeak-feeds" "emacspeak-feeds.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-feeds.el

(let ((loads (get 'emacspeak-feeds 'custom-loads))) (if (member '"emacspeak-feeds" loads) nil (put 'emacspeak-feeds 'custom-loads (cons '"emacspeak-feeds" loads))))

(defvar emacspeak-feeds-feeds-table (make-hash-table :test #'equal) "\
Hash table to enable efficient feed look up when adding feeds.")

(autoload 'emacspeak-feeds-archive-feeds "emacspeak-feeds" "\
Archive list of subscribed fees to personal resource directory.
Archiving is useful when synchronizing feeds across multiple machines.

\(fn)" t nil)

(autoload 'emacspeak-feeds-restore-feeds "emacspeak-feeds" "\
Restore list of subscribed fees from  personal resource directory.
Archiving is useful when synchronizing feeds across multiple machines.

\(fn)" t nil)

(autoload 'emacspeak-feeds-fastload-feeds "emacspeak-feeds" "\
Fast load list of feeds from archive.
This directly  updates emacspeak-feeds from the archive, rather than adding those entries to the current set of subscribed feeds.

\(fn)" t nil)

(autoload 'emacspeak-feeds-rss-display "emacspeak-feeds" "\
Display RSS feed.

\(fn FEED-URL)" t nil)

(autoload 'emacspeak-feeds-opml-display "emacspeak-feeds" "\
Display OPML feed.

\(fn FEED-URL)" t nil)

(autoload 'emacspeak-feeds-atom-display "emacspeak-feeds" "\
Display ATOM feed.

\(fn FEED-URL)" t nil)

(autoload 'emacspeak-feeds-browse "emacspeak-feeds" "\
Browse specified  feed.

\(fn FEED)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-feeds" '("emacspeak-")))

;;;***

;;;### (autoloads nil "emacspeak-filtertext" "emacspeak-filtertext.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-filtertext.el

(autoload 'emacspeak-filtertext "emacspeak-filtertext" "\
Copy over text in region to special filtertext buffer in
preparation for interactively filtering text. 

\(fn START END)" t nil)

(autoload 'emacspeak-filtertext-revert "emacspeak-filtertext" "\
Revert to original text.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-filtertext" '("emacspeak-filtertext-")))

;;;***

;;;### (autoloads nil "emacspeak-find-func" "emacspeak-find-func.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-find-func.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-find-func" '("emacspeak-find-func-commands")))

;;;***

;;;### (autoloads nil "emacspeak-finder-inf" "emacspeak-finder-inf.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-finder-inf.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-finder-inf" '("finder-package-info")))

;;;***

;;;### (autoloads nil "emacspeak-fix-interactive" "emacspeak-fix-interactive.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-fix-interactive.el

(autoload 'emacspeak-should-i-fix-interactive-p "emacspeak-fix-interactive" "\
Predicate to test if this function should be fixed. 

\(fn SYM)" nil nil)

(autoload 'emacspeak-fix-interactive-command-if-necessary "emacspeak-fix-interactive" "\
Fix command if necessary.

\(fn COMMAND)" nil nil)

(autoload 'emacspeak-fix-commands-loaded-from "emacspeak-fix-interactive" "\
Fix all commands loaded from a specified module.

\(fn MODULE)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-fix-interactive" '("emacspeak-" "ems-prompt-without-minibuffer-p")))

;;;***

;;;### (autoloads nil "emacspeak-flyspell" "emacspeak-flyspell.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-flyspell.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-flyspell" '("emacspeak-flyspell-correct")))

;;;***

;;;### (autoloads nil "emacspeak-forms" "emacspeak-forms.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-forms.el

(autoload 'emacspeak-forms-speak-field "emacspeak-forms" "\
Speak current form field name and value.
Assumes that point is at the front of a field value.

\(fn)" t nil)

(autoload 'emacspeak-forms-find-file "emacspeak-forms" "\
Visit a forms file

\(fn FILENAME)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-forms" '("emacspeak-forms-")))

;;;***

;;;### (autoloads nil "emacspeak-gnus" "emacspeak-gnus.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-gnus.el

(autoload 'emacspeak-gnus-personal-gmail-recent "emacspeak-gnus" "\
Look for mail addressed personally in the last day.

\(fn)" t nil)

(autoload 'emacspeak-gnus-personal-gmail-last-week "emacspeak-gnus" "\
Look for mail addressed personally in the last week.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-gnus" '("emacspeak-gnus-" "gnus-summary-downcase-article")))

;;;***

;;;### (autoloads nil "emacspeak-gomoku" "emacspeak-gomoku.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-gomoku.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-gomoku" '("emacspeak-gomoku-" "gomoku-")))

;;;***

;;;### (autoloads nil "emacspeak-google" "emacspeak-google.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-google.el

(define-prefix-command 'emacspeak-google-command 'emacspeak-google-keymap)

(autoload 'emacspeak-google-tts "emacspeak-google" "\
Speak text using Google Network TTS.
Optional interactive prefix arg `lang' specifies  language identifier.

\(fn TEXT &optional LANG)" t nil)

(autoload 'emacspeak-google-tts-region "emacspeak-google" "\
Speak region using Google Network TTS.

\(fn START END &optional ASK-LANG)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-google" '("emacspeak-google-")))

;;;***

;;;### (autoloads nil "emacspeak-gridtext" "emacspeak-gridtext.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-gridtext.el

(autoload 'emacspeak-gridtext-load "emacspeak-gridtext" "\
Load saved grid settings.

\(fn FILE)" t nil)

(autoload 'emacspeak-gridtext-save "emacspeak-gridtext" "\
Save out grid settings.

\(fn FILE)" t nil)

(autoload 'emacspeak-gridtext-apply "emacspeak-gridtext" "\
Apply grid to region.

\(fn START END GRID)" t nil)

(define-prefix-command 'emacspeak-gridtext 'emacspeak-gridtext-keymap)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-gridtext" '("emacspeak-gridtext-")))

;;;***

;;;### (autoloads nil "emacspeak-helm" "emacspeak-helm.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-helm.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-helm" '("emacspeak-helm-")))

;;;***

;;;### (autoloads nil "emacspeak-hide" "emacspeak-hide.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-hide.el

(autoload 'emacspeak-hide-all-blocks-in-buffer "emacspeak-hide" "\
Hide all blocks in current buffer.

\(fn)" nil nil)

(autoload 'emacspeak-hide-or-expose-block "emacspeak-hide" "\
Hide or expose a block of text.
This command either hides or exposes a block of text
starting on the current line.  A block of text is defined as
a portion of the buffer in which all lines start with a
common PREFIX.  Optional interactive prefix arg causes all
blocks in current buffer to be hidden or exposed.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-hide-or-expose-all-blocks "emacspeak-hide" "\
Hide or expose all blocks in buffer.

\(fn)" t nil)

(autoload 'emacspeak-hide-speak-block-sans-prefix "emacspeak-hide" "\
Speaks current block after stripping its prefix.
If the current block is not hidden, it first hides it.
This is useful because as you locate blocks, you can invoke this
command to listen to the block,
and when you have heard enough navigate easily  to move past the block.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-hide" '("emacspeak-hid")))

;;;***

;;;### (autoloads nil "emacspeak-hydra" "emacspeak-hydra.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-hydra.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-hydra" '("emacspeak-hydra-")))

;;;***

;;;### (autoloads nil "emacspeak-ibuffer" "emacspeak-ibuffer.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-ibuffer.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-ibuffer" '("emacspeak-ibuffer-s")))

;;;***

;;;### (autoloads nil "emacspeak-ido" "emacspeak-ido.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ido.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-ido" '("emacspeak-ido-")))

;;;***

;;;### (autoloads nil "emacspeak-imenu" "emacspeak-imenu.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-imenu.el

(autoload 'emacspeak-imenu-goto-next-index-position "emacspeak-imenu" "\
Goto the next index position in current buffer

\(fn)" t nil)

(autoload 'emacspeak-imenu-goto-previous-index-pos "emacspeak-imenu" "\
Goto the previous index pos in current buffer

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-imenu" '("emacspeak-imenu-")))

;;;***

;;;### (autoloads nil "emacspeak-info" "emacspeak-info.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-info.el

(autoload 'emacspeak-info-wizard "emacspeak-info" "\
Read a node spec from the minibuffer and launch
Info-goto-node.
See documentation for command `Info-goto-node' for details on
node-spec.

\(fn NODE-SPEC)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-info" '("emacspeak-info-")))

;;;***

;;;### (autoloads nil "emacspeak-ispell" "emacspeak-ispell.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-ispell.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-ispell" '("emacspeak-ispell-max-choices")))

;;;***

;;;### (autoloads nil "emacspeak-ivy" "emacspeak-ivy.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ivy.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-ivy" '("emacspeak-ivy-speak-selection")))

;;;***

;;;### (autoloads nil "emacspeak-jabber" "emacspeak-jabber.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-jabber.el

(autoload 'emacspeak-jabber-speak-recent-message "emacspeak-jabber" "\
Speak most recent message if one exists.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-jabber" '("emacspeak-jabber-")))

;;;***

;;;### (autoloads nil "emacspeak-js2" "emacspeak-js2.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-js2.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-js2" '("emacspeak-js2-hook")))

;;;***

;;;### (autoloads nil "emacspeak-keymap" "emacspeak-keymap.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-keymap.el

(autoload 'emacspeak-keymap-command-p "emacspeak-keymap" "\
Check if `s' is suitable to be bound to a key.

\(fn S)" nil nil)

(autoload 'emacspeak-keymap-update "emacspeak-keymap" "\
Update keymap with specified binding.

\(fn KEYMAP BINDING)" nil nil)

(defvar emacspeak-keymap nil "\
Primary keymap used by emacspeak. ")

(autoload 'emacspeak-keymap-choose-new-emacspeak-prefix "emacspeak-keymap" "\
Interactively select a new prefix key to use for all emacspeak
commands.  The default is to use `C-e'  This command
lets you switch the prefix to something else.  This is a useful thing
to do if you run emacspeak on a remote machine from inside a terminal
that is running inside a local emacspeak session.  You can have the
remote emacspeak use a different control key to give your fingers some
relief.

\(fn PREFIX-KEY)" t nil)

(autoload 'emacspeak-keymap-remove-emacspeak-edit-commands "emacspeak-keymap" "\
We define keys that invoke editing commands to be undefined

\(fn KEYMAP)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-keymap" '("emacspeak-")))

;;;***

;;;### (autoloads nil "emacspeak-librivox" "emacspeak-librivox.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-librivox.el

(autoload 'emacspeak-librivox-search-by-genre "emacspeak-librivox" "\
Search by genre.
Optional prefix arg `offset' prompts for offset.

\(fn GENRE &optional OFFSET)" t nil)

(autoload 'emacspeak-librivox-search-by-author "emacspeak-librivox" "\
Search by author. Both exact and partial matches for
`author'. Optional interactive prefix arg `offset' prompts for offset
--- use this for retrieving next set of results.

\(fn AUTHOR &optional OFFSET)" t nil)

(autoload 'emacspeak-librivox-search-by-title "emacspeak-librivox" "\
Search by title. Both exact and partial matches for `title'. Optional
prefix arg `offset' prompts for offset --- use this for retrieving
more results.

\(fn TITLE &optional OFFSET)" t nil)

(autoload 'emacspeak-librivox "emacspeak-librivox" "\
Launch a Librivox Search.

\(fn SEARCH-TYPE)" t nil)

(autoload 'emacspeak-librivox-play "emacspeak-librivox" "\
Play book stream

\(fn RSS-URL)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-librivox" '("emacspeak-librivox-")))

;;;***

;;;### (autoloads nil "emacspeak-lispy" "emacspeak-lispy.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-lispy.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-lispy" '("emacspeak-lispy-setup")))

;;;***

;;;### (autoloads nil "emacspeak-load-path" "emacspeak-load-path.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-load-path.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-load-path" '("emacspeak-" "ems-")))

;;;***

;;;### (autoloads nil "emacspeak-m-player" "emacspeak-m-player.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-m-player.el

(let ((loads (get 'emacspeak-m-player 'custom-loads))) (if (member '"emacspeak-m-player" loads) nil (put 'emacspeak-m-player 'custom-loads (cons '"emacspeak-m-player" loads))))

(autoload 'emacspeak-multimedia "emacspeak-m-player" "\
Start or control Emacspeak multimedia player.

Uses current context to prompt for media to play.
Controls media playback when already playing a stream.

\\{emacspeak-m-player-mode-map}.

\(fn)" t nil)

(autoload 'emacspeak-m-player-pop-to-player "emacspeak-m-player" "\
Pop to m-player buffer.

\(fn)" t nil)

(defvar emacspeak-m-player-playlist-pattern (concat (regexp-opt (list ".m3u" ".asx" ".pls" ".rpm" ".ram")) "$") "\
Pattern for matching playlists.")

(autoload 'emacspeak-m-player-bind-accelerator "emacspeak-m-player" "\
Binds key to invoke m-player  on specified directory.

\(fn DIRECTORY KEY)" t nil)

(autoload 'emacspeak-m-player-accelerator "emacspeak-m-player" "\
Launch MPlayer on specified directory.

\(fn DIRECTORY)" nil nil)

(autoload 'emacspeak-media-guess-directory "emacspeak-m-player" "\
Guess default directory.

\(fn)" nil nil)

(autoload 'emacspeak-m-player-url "emacspeak-m-player" "\
Call emacspeak-m-player with specified URL.

\(fn URL &optional PLAYLIST-P)" t nil)

(defvar emacspeak-m-player-file-list nil "\
List  that records list of files being played.")

(autoload 'emacspeak-media-read-resource "emacspeak-m-player" "\
Read resource from minibuffer with contextual smarts.

\(fn)" nil nil)

(autoload 'emacspeak-m-player "emacspeak-m-player" "\
Play specified resource using m-player.  Optional prefix argument
play-list interprets resource as a play-list.  Second interactive
prefix arg adds option -allow-dangerous-playlist-parsing to mplayer.
Resource is a media resource or playlist containing media resources.
The player is placed in a buffer in emacspeak-m-player-mode.

\(fn RESOURCE &optional PLAY-LIST)" t nil)

(autoload 'emacspeak-m-player-using-openal "emacspeak-m-player" "\
Use openal as the audio output driver. Adding hrtf=true to
~/.alsoftrc gives HRTF. You need to have openal installed and have an
mplayer that has been compiled with openal support to use this
feature. Calling spec is like `emacspeak-m-player'.

\(fn RESOURCE &optional PLAY-LIST)" t nil)

(autoload 'emacspeak-m-player-using-hrtf "emacspeak-m-player" "\
Add af resample=48000,hrtf to startup options.
This will work if the soundcard is set to 48000.

\(fn)" t nil)

(autoload 'emacspeak-m-player-shuffle "emacspeak-m-player" "\
Launch M-Player with shuffle turned on.

\(fn)" t nil)

(autoload 'emacspeak-m-player-load "emacspeak-m-player" "\
Load specified resource into a running  m-player.
Interactive prefix arg appends the new resource to what is playing.

\(fn RESOURCE &optional APPEND)" t nil)

(autoload 'emacspeak-m-player-volume-up "emacspeak-m-player" "\
Increase volume.

\(fn)" t nil)

(autoload 'emacspeak-m-player-volume-down "emacspeak-m-player" "\
Decrease volume.

\(fn)" t nil)

(autoload 'emacspeak-m-player-volume-change "emacspeak-m-player" "\
Change volume to specified absolute value.

\(fn VALUE)" t nil)

(autoload 'emacspeak-m-player-balance "emacspeak-m-player" "\
Set left/right balance.

\(fn)" t nil)

(autoload 'emacspeak-m-player-slave-command "emacspeak-m-player" "\
Dispatch slave command read from minibuffer.

\(fn COMMAND)" t nil)

(autoload 'emacspeak-m-player-display-percent "emacspeak-m-player" "\
Display current percentage.

\(fn)" t nil)

(autoload 'emacspeak-m-player-stream-info "emacspeak-m-player" "\
Speak and display metadata if available.
Interactive prefix arg toggles automatic cueing of ICY info updates.

\(fn &optional TOGGLE-CUE)" t nil)

(autoload 'emacspeak-m-player-get-length "emacspeak-m-player" "\
Display length of track in seconds.

\(fn)" t nil)

(autoload 'ems--m-p-get-yt-audio-fmt "emacspeak-m-player" "\
Get first available audio format code for   YT URL

\(fn URL)" nil nil)

(autoload 'emacspeak-m-player-pause-or-resume "emacspeak-m-player" "\
Pause/resume if m-player is running. For use  in
emacspeak-silence-hook.

\(fn)" nil nil)

(autoload 'emacspeak-m-player-amark-add "emacspeak-m-player" "\
Set AMark `name' at current position in current audio stream.
Interactive prefix arg prompts for position.
As the default, use current position.

\(fn NAME &optional PROMPT-POSITION)" t nil)

(autoload 'emacspeak-m-player-amark-jump "emacspeak-m-player" "\
Jump to specified AMark.

\(fn)" t nil)

(autoload 'emacspeak-m-player-play-rss "emacspeak-m-player" "\
Play an RSS stream by converting to  an M3U playlist.

\(fn RSS-URL)" t nil)

(autoload 'emacspeak-m-player-locate-media "emacspeak-m-player" "\
Locate media matching specified pattern.  The results can be
played as a play-list by pressing [RET] on the first line.
Pattern is first converted to a regexp that accepts common
punctuation separators (-,._'\") in place of white-space.
Results are placed in a Locate buffer and can be played using
M-Player --- use \\[emacspeak-dired-open-this-file] locally bound to C-RET 
to play individual tracks.

\(fn PATTERN)" t nil)

(autoload 'emacspeak-m-player-add-ladspa "emacspeak-m-player" "\
Apply plugin to running MPlayer.

\(fn)" t nil)

(autoload 'emacspeak-m-player-delete-ladspa "emacspeak-m-player" "\
Delete plugin from  running MPlayer.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-m-player" '("emacspeak-" "ems-")))

;;;***

;;;### (autoloads nil "emacspeak-magit" "emacspeak-magit.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-magit.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-magit" '("emacspeak-magit-blame-speak")))

;;;***

;;;### (autoloads nil "emacspeak-man" "emacspeak-man.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-man.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-man" '("emacspeak-man-")))

;;;***

;;;### (autoloads nil "emacspeak-maths" "emacspeak-maths.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-maths.el

(autoload 'emacspeak-maths-start "emacspeak-maths" "\
Start Maths server bridge.

\(fn)" t nil)

(autoload 'emacspeak-maths-enter-guess "emacspeak-maths" "\
Send the guessed  LaTeX expression to Maths server.
Guess is based on context.

\(fn)" t nil)

(autoload 'emacspeak-maths-enter "emacspeak-maths" "\
Send a LaTeX expression to Maths server.
Tries to guess default based on context.
Uses guessed default if user enters an empty string.

\(fn LATEX)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-maths" '("emacspeak-maths")))

;;;***

;;;### (autoloads nil "emacspeak-message" "emacspeak-message.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-message.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-message" '("emacspeak-message-punctuation-mode")))

;;;***

;;;### (autoloads nil "emacspeak-midge" "emacspeak-midge.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-midge.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-midge" '("midge-mode-hook")))

;;;***

;;;### (autoloads nil "emacspeak-mines" "emacspeak-mines.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-mines.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-mines" '("emacspeak-mines-")))

;;;***

;;;### (autoloads nil "emacspeak-muggles" "emacspeak-muggles.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-muggles.el

(autoload 'emacspeak-muggles-generate "emacspeak-muggles" "\
Generate a Muggle from specified k-map.
Argument `k-map' is a symbol  that names a keymap.

\(fn K-MAP)" nil nil)

(autoload 'emacspeak-muggles-ido-yank "emacspeak-muggles" "\
Pick what to yank using ido completion.

\(fn)" t nil)

(autoload 'emacspeak-muggles-generate-autoloads "emacspeak-muggles" "\
Generate autoload lines for all defined muggles.
Also generates global keybindings if any.

\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-muggles" '("emacspeak-muggles-")))

;;;***

;;;### (autoloads nil "emacspeak-net-utils" "emacspeak-net-utils.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-net-utils.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-net-utils" '("emacspeak-net-utils-commands")))

;;;***

;;;### (autoloads nil "emacspeak-newsticker" "emacspeak-newsticker.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-newsticker.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-newsticker" '("emacspeak-newsticker-summarize-item")))

;;;***

;;;### (autoloads nil "emacspeak-npr" "emacspeak-npr.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-npr.el

(autoload 'emacspeak-npr-view "emacspeak-npr" "\
View results as Atom.

\(fn OPERATION OPERAND)" nil nil)

(autoload 'emacspeak-npr-listing "emacspeak-npr" "\
Display specified listing.
Interactive prefix arg prompts for search.

\(fn &optional SEARCH)" t nil)

(autoload 'emacspeak-npr-play-program "emacspeak-npr" "\
Play specified NPR program.
Optional interactive prefix arg prompts for a date.

\(fn PID &optional GET-DATE)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-npr" '("emacspeak-npr-")))

;;;***

;;;### (autoloads nil "emacspeak-nxml" "emacspeak-nxml.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-nxml.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-nxml" '("emacspeak-nxml-summarize-outline")))

;;;***

;;;### (autoloads nil "emacspeak-ocr" "emacspeak-ocr.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ocr.el

(autoload 'emacspeak-ocr-customize "emacspeak-ocr" "\
Customize OCR settings.

\(fn)" t nil)

(autoload 'emacspeak-ocr "emacspeak-ocr" "\
An OCR front-end for the Emacspeak desktop.  

Page image is acquired using tools from the SANE package.
The acquired image is run through the OCR engine if one is
available, and the results placed in a buffer that is
suitable for browsing the results.

For detailed help, invoke command emacspeak-ocr bound to
\\[emacspeak-ocr] to launch emacspeak-ocr-mode, and press
`?' to display mode-specific help for emacspeak-ocr-mode.

\(fn)" t nil)

(autoload 'emacspeak-ocr-name-document "emacspeak-ocr" "\
Name document being scanned in the current OCR buffer.
Pick a short but meaningful name.

\(fn NAME)" t nil)

(autoload 'emacspeak-ocr-scan-image "emacspeak-ocr" "\
Acquire page image.

\(fn)" t nil)

(autoload 'emacspeak-ocr-scan-photo "emacspeak-ocr" "\
Scan in a photograph.
The scanned image is converted to JPEG.

\(fn &optional METADATA)" t nil)

(autoload 'emacspeak-ocr-write-document "emacspeak-ocr" "\
Writes out recognized text from all pages in current document.

\(fn)" t nil)

(autoload 'emacspeak-ocr-save-current-page "emacspeak-ocr" "\
Writes out recognized text from current page
to an appropriately named file.

\(fn)" t nil)

(autoload 'emacspeak-ocr-recognize-image "emacspeak-ocr" "\
Run OCR engine on current image.
Prompts for image file if file corresponding to the expected
`current page' is not found.

\(fn)" t nil)

(autoload 'emacspeak-ocr-scan-and-recognize "emacspeak-ocr" "\
Scan in a page and run OCR engine on it.
Use this command once you've verified that the separate
steps of acquiring an image and running the OCR engine work
correctly by themselves.

\(fn)" t nil)

(autoload 'emacspeak-ocr-open-working-directory "emacspeak-ocr" "\
Launch dired on OCR working directory.

\(fn)" t nil)

(autoload 'emacspeak-ocr-forward-page "emacspeak-ocr" "\
Like forward page, but tracks page number of current document.

\(fn &optional COUNT-IGNORED)" t nil)

(autoload 'emacspeak-ocr-backward-page "emacspeak-ocr" "\
Like backward page, but tracks page number of current document.

\(fn &optional COUNT-IGNORED)" t nil)

(autoload 'emacspeak-ocr-page "emacspeak-ocr" "\
Move to specified page.

\(fn)" t nil)

(autoload 'emacspeak-ocr-read-current-page "emacspeak-ocr" "\
Speaks current page.

\(fn)" t nil)

(autoload 'emacspeak-ocr-set-scan-image-options "emacspeak-ocr" "\
Interactively update scan image options.
Prompts with current setting in the minibuffer.
Setting persists for current Emacs session.

\(fn SETTING)" t nil)

(autoload 'emacspeak-ocr-set-compress-image-options "emacspeak-ocr" "\
Interactively update  image compression options.
Prompts with current setting in the minibuffer.
Setting persists for current Emacs session.

\(fn SETTING)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-ocr" '("emacspeak-ocr-")))

;;;***

;;;### (autoloads nil "emacspeak-org" "emacspeak-org.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-org.el

(autoload 'emacspeak-org-popup-input "emacspeak-org" "\
Pops up an org input area.

\(fn)" t nil)

(autoload 'emacspeak-org-bookmark "emacspeak-org" "\
Bookmark from org.

\(fn &optional GOTO)" t nil)

(autoload 'emacspeak-org-table-speak-current-element "emacspeak-org" "\
echoes current table element

\(fn)" t nil)

(autoload 'emacspeak-org-table-speak-column-header "emacspeak-org" "\
echoes column header

\(fn)" t nil)

(autoload 'emacspeak-org-table-speak-row-header "emacspeak-org" "\
echoes row header

\(fn)" t nil)

(autoload 'emacspeak-org-table-speak-coordinates "emacspeak-org" "\
echoes coordinates

\(fn)" t nil)

(autoload 'emacspeak-org-table-speak-both-headers-and-element "emacspeak-org" "\
echoes both row and col headers.

\(fn)" t nil)

(autoload 'emacspeak-org-table-speak-row-header-and-element "emacspeak-org" "\
echoes row header and element

\(fn)" t nil)

(autoload 'emacspeak-org-table-speak-column-header-and-element "emacspeak-org" "\
echoes col header and element

\(fn)" t nil)

(unless (fboundp 'org-table-previous-row) (defun org-table-previous-row nil "Go to the previous row (same column) in the current table.\nBefore doing so, re-align the table if necessary." (interactive) (org-table-maybe-eval-formula) (org-table-maybe-recalculate-line) (if (or (looking-at "[ \11]*$") (save-excursion (skip-chars-backward " \11") (bolp))) (newline) (if (and org-table-automatic-realign org-table-may-need-update) (org-table-align)) (let ((col (org-table-current-column))) (beginning-of-line 0) (when (or (not (org-at-table-p)) (org-at-table-hline-p)) (beginning-of-line 1)) (org-table-goto-column col) (skip-chars-backward "^|\n\15") (if (looking-at " ") (forward-char 1))))))

(autoload 'emacspeak-org-capture-link "emacspeak-org" "\
Capture hyperlink to current context.
To use this command, first  do `customize-variable' `org-capture-template'
and assign  letter `h' to a template that creates the hyperlink on capture.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-org" '("emacspeak-org-" "org-eww-store-link")))

;;;***

;;;### (autoloads nil "emacspeak-origami" "emacspeak-origami.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-origami.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-origami" '("emacspeak-origami-invisible-p")))

;;;***

;;;### (autoloads nil "emacspeak-outline" "emacspeak-outline.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-outline.el

(autoload 'emacspeak-outline-speak-next-heading "emacspeak-outline" "\
Analogous to outline-next-visible-heading,
except that the outline section is  spoken

\(fn)" t nil)

(autoload 'emacspeak-outline-speak-previous-heading "emacspeak-outline" "\
Analogous to outline-previous-visible-heading,
except that the outline section is  spoken

\(fn)" t nil)

(autoload 'emacspeak-outline-speak-forward-heading "emacspeak-outline" "\
Analogous to outline-forward-same-level,
except that the outline section is  spoken

\(fn)" t nil)

(autoload 'emacspeak-outline-speak-backward-heading "emacspeak-outline" "\
Analogous to outline-backward-same-level
except that the outline section is  spoken

\(fn)" t nil)

(autoload 'emacspeak-outline-speak-this-heading "emacspeak-outline" "\
Speak current outline section starting from point

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-outline" '("emacspeak-outline-")))

;;;***

;;;### (autoloads nil "emacspeak-package" "emacspeak-package.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-package.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-package" '("emacspeak-package-")))

;;;***

;;;### (autoloads nil "emacspeak-paradox" "emacspeak-paradox.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-paradox.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-paradox" '("emacspeak-paradox-")))

;;;***

;;;### (autoloads nil "emacspeak-personality" "emacspeak-personality.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-personality.el

(autoload 'emacspeak-personality-add "emacspeak-personality" "\
Apply personality VOICE to specified region,
over-writing any current personality settings.

\(fn START END VOICE &optional OBJECT)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-personality" '("emacspeak-personality-remove" "ems--voiceify-overlays")))

;;;***

;;;### (autoloads nil "emacspeak-pianobar" "emacspeak-pianobar.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-pianobar.el

(autoload 'emacspeak-pianobar-electric-mode-toggle "emacspeak-pianobar" "\
Toggle electric mode in pianobar buffer.
If electric mode is on, keystrokes invoke pianobar commands directly.

\(fn)" t nil)

(autoload 'emacspeak-pianobar "emacspeak-pianobar" "\
Start or control Emacspeak Pianobar player.

\(fn)" t nil)

(defvar emacspeak-pianobar-max-preset 10 "\
Number of presets.")

(custom-autoload 'emacspeak-pianobar-max-preset "emacspeak-pianobar" t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-pianobar" '("emacspeak-pianobar-")))

;;;***

;;;### (autoloads nil "emacspeak-popup" "emacspeak-popup.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-popup.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-popup" '("emacspeak-popup-speak-item")))

;;;***

;;;### (autoloads nil "emacspeak-preamble" "emacspeak-preamble.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-preamble.el

(autoload 'auth-source-pass-enable "emacspeak-preamble" "\
Enable auth-source-password-store.

\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-preamble" '("auth-source-pass-")))

;;;***

;;;### (autoloads nil "emacspeak-proced" "emacspeak-proced.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-proced.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-proced" '("emacspeak-proced-")))

;;;***

;;;### (autoloads nil "emacspeak-projectile" "emacspeak-projectile.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-projectile.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-projectile" '("emacspeak-projectile-file-action")))

;;;***

;;;### (autoloads nil "emacspeak-prompts" "emacspeak-prompts.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-prompts.el

(autoload 'emacspeak-prompt "emacspeak-prompts" "\
Play  prompt for specified name.

\(fn NAME)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-prompts" '("emacspeak-prompts-")))

;;;***

;;;### (autoloads nil "emacspeak-pronounce" "emacspeak-pronounce.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-pronounce.el

(autoload 'emacspeak-pronounce-add-dictionary-entry "emacspeak-pronounce" "\
Add dictionary entry.
This adds pronunciation pair
STRING.PRONUNCIATION to the dictionary.
Argument KEY specifies a dictionary key e.g. directory, mode etc.

\(fn KEY STRING PRONUNCIATION)" nil nil)

(autoload 'emacspeak-pronounce-save-dictionaries "emacspeak-pronounce" "\
Writes out the persistent emacspeak pronunciation dictionaries.

\(fn)" t nil)

(autoload 'emacspeak-pronounce-load-dictionaries "emacspeak-pronounce" "\
Load pronunciation dictionaries.
Optional argument FILENAME specifies the dictionary file.

\(fn &optional FILENAME)" t nil)

(autoload 'emacspeak-pronounce-clear-dictionaries "emacspeak-pronounce" "\
Clear all current pronunciation dictionaries.

\(fn)" t nil)

(autoload 'emacspeak-pronounce-yank-word "emacspeak-pronounce" "\
Yank word at point into minibuffer.

\(fn)" t nil)

(autoload 'emacspeak-pronounce-define-local-pronunciation "emacspeak-pronounce" "\
Define buffer local pronunciation.
Argument WORD specifies the word which should be pronounced as specified by PRONUNCIATION.

\(fn WORD PRONUNCIATION)" t nil)

(autoload 'emacspeak-pronounce-define-template-pronunciation "emacspeak-pronounce" "\
Interactively define template entries in the pronunciation dictionaries.
Default term to define is delimited by region.
First loads any persistent dictionaries if not already loaded.

\(fn)" t nil)

(autoload 'emacspeak-pronounce-define-pronunciation "emacspeak-pronounce" "\
Interactively define entries in the pronunciation dictionaries.
Default term to define is delimited by region.
First loads any persistent dictionaries if not already loaded.

\(fn)" t nil)

(autoload 'emacspeak-pronounce-pronunciation-table "emacspeak-pronounce" "\
Closure that returns the pronunciation table.

\(fn)" nil nil)

(autoload 'emacspeak-pronounce-toggle-use-of-dictionaries "emacspeak-pronounce" "\
Toggle use of pronunciation dictionaries in current buffer.
Pronunciations can be defined on a per file, per directory and/or
per mode basis.  Pronunciations are activated on a per buffer
basis.  Turning on the use of pronunciation dictionaries results
in emacspeak composing a pronunciation table based on the
currently defined pronunciation dictionaries.  After this, the
pronunciations will be applied whenever text in the buffer is
spoken.  Optional argument state can be used from Lisp programs
to explicitly turn pronunciations on or off.

\(fn &optional STATE)" t nil)

(autoload 'emacspeak-pronounce-refresh-pronunciations "emacspeak-pronounce" "\
Refresh pronunciation table for current buffer.
Activates pronunciation dictionaries if not already active.

\(fn)" t nil)

(autoload 'emacspeak-pronounce-edit-pronunciations "emacspeak-pronounce" "\
Prompt for and launch a pronunciation editor on the
specified pronunciation dictionary key.

\(fn KEY)" t nil)

(autoload 'emacspeak-pronounce-dispatch "emacspeak-pronounce" "\
Provides the user interface front-end to Emacspeak's pronunciation dictionaries.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-pronounce" '("emacspeak-pronounce-")))

;;;***

;;;### (autoloads nil "emacspeak-redefine" "emacspeak-redefine.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-redefine.el

(autoload 'emacspeak-self-insert-command "emacspeak-redefine" "\
Insert a character.
Speaks the character if emacspeak-character-echo is true.
See  command emacspeak-toggle-word-echo bound to
\\[emacspeak-toggle-word-echo].
Speech flushes as you type.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-forward-char "emacspeak-redefine" "\
Forward-char redefined to speak char moved to. 

\(fn &optional ARG)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-redefine" '("emacspeak-")))

;;;***

;;;### (autoloads nil "emacspeak-remote" "emacspeak-remote.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-remote.el

(let ((loads (get 'emacspeak-remote 'custom-loads))) (if (member '"emacspeak-remote" loads) nil (put 'emacspeak-remote 'custom-loads (cons '"emacspeak-remote" loads))))

(defvar emacspeak-remote-hooks nil "\
List of hook functions that are run after
emacspeak is set to run as a remote application.
Use this to add actions you typically perform after you enter remote
mode.")

(custom-autoload 'emacspeak-remote-hooks "emacspeak-remote" t)

(autoload 'emacspeak-remote-edit-current-remote-hostname "emacspeak-remote" "\
Interactively set up where we came from.
Value is persisted for use with ssh servers.

\(fn)" t nil)

(autoload 'emacspeak-remote-quick-connect-to-server "emacspeak-remote" "\
Connect to remote server.
Does not prompt for host or port, but quietly uses the guesses
that appear as defaults when prompting. Use this once you are
sure the guesses are usually correct.

\(fn)" t nil)

(autoload 'emacspeak-remote-home "emacspeak-remote" "\
Open ssh session to where we came from.
Uses value returned by `emacspeak-remote-get-current-remote-hostname'.

\(fn)" t nil)

(autoload 'emacspeak-remote-ssh-to-server "emacspeak-remote" "\
Open ssh session to where we came from.

\(fn LOGIN HOST PORT)" t nil)

(defvar emacspeak-remote-default-ssh-server nil "\
Default ssh server to use for remote speech server.")

(custom-autoload 'emacspeak-remote-default-ssh-server "emacspeak-remote" t)

(autoload 'emacspeak-remote-quick-connect-via-ssh "emacspeak-remote" "\
Connect via ssh to remote Emacspeak server.
Server is specified via custom option `emacspeak-remote-default-ssh-server'.

\(fn)" t nil)

(autoload 'emacspeak-remote-connect-to-server "emacspeak-remote" "\
Connect to and start using remote speech server running on
host host and listening on port port. Host is the hostname of the
remote server, typically the desktop machine. Port is the tcp
port that that host is listening on for speech requests.

\(fn HOST PORT)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-remote" '("emacspeak-remote-")))

;;;***

;;;### (autoloads nil "emacspeak-rmail" "emacspeak-rmail.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-rmail.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-rmail" '("emacspeak-rmail-summarize-")))

;;;***

;;;### (autoloads nil "emacspeak-rpm-spec" "emacspeak-rpm-spec.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-rpm-spec.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-rpm-spec" '("emacspeak-rpm-spec-")))

;;;***

;;;### (autoloads nil "emacspeak-sage" "emacspeak-sage.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-sage.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-sage" '("emacspeak-sage-")))

;;;***

;;;### (autoloads nil "emacspeak-ses" "emacspeak-ses.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-ses.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-ses" '("emacspeak-ses-")))

;;;***

;;;### (autoloads nil "emacspeak-setup" "emacspeak-setup.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-setup.el

(defvar emacspeak-directory (expand-file-name "../" (file-name-directory load-file-name)) "\
Directory where emacspeak is installed. ")

(defvar emacspeak-lisp-directory (expand-file-name "lisp/" emacspeak-directory) "\
Directory where emacspeak lisp files are  installed. ")

(defvar emacspeak-sounds-directory (expand-file-name "sounds/" emacspeak-directory) "\
Directory containing auditory icons for Emacspeak.")

(defvar emacspeak-xslt-directory (expand-file-name "xsl/" emacspeak-directory) "\
Directory holding XSL transformations.")

(defvar emacspeak-etc-directory (expand-file-name "etc/" emacspeak-directory) "\
Directory containing miscellaneous files  for Emacspeak.")

(defvar emacspeak-servers-directory (expand-file-name "servers/" emacspeak-directory) "\
Directory containing speech servers  for Emacspeak.")

(defvar emacspeak-info-directory (expand-file-name "info/" emacspeak-directory) "\
Directory containing  Emacspeak info files.")

(defvar emacspeak-resource-directory (expand-file-name "~/.emacspeak/") "\
Directory where Emacspeak resource files
such as pronunciation dictionaries are stored. ")

(defvar emacspeak-readme-file (expand-file-name "README" emacspeak-directory) "\
README file from where we get Git  revision number.")

(defvar emacspeak-media-extensions (let ((ext '("mov" "wma" "wmv" "flv" "m4a" "m4b" "flac" "aiff" "aac" "opus " "mkv" "ogv" "oga" "ogg" "mp3" "mp4" "webm" "wav"))) (concat "\\." (regexp-opt (nconc ext (mapcar #'upcase ext)) 'parens) "$")) "\
Extensions that match media files.")

(defvar dtk-startup-hook '(emacspeak-tts-startup-hook emacspeak-tts-notify-hook) "\
List of hooks to be run after starting up the speech server.
Set things like speech rate, punctuation mode etc in this
hook.")

(custom-autoload 'dtk-startup-hook "emacspeak-setup" t)

(autoload 'emacspeak-tts-startup-hook "emacspeak-setup" "\
Default hook function run after TTS is started.

\(fn)" nil nil)

(defvar tts-notification-device (cl-first (split-string (shell-command-to-string "aplay -L 2>/dev/null | grep mono"))) "\
Virtual ALSA device to use for notifications stream.")

(custom-autoload 'tts-notification-device "emacspeak-setup" t)

(autoload 'emacspeak-tts-multistream-p "emacspeak-setup" "\
Checks if this tts-engine can support multiple streams.

\(fn TTS-ENGINE)" nil nil)

(autoload 'emacspeak-setup-header-line "emacspeak-setup" "\
Set up Emacspeak to speak a default header line.

\(fn)" nil nil)

(defvar emacspeak-startup-hook '(emacspeak-setup-header-line emacspeak-turn-off-visual-line-mode) "\
Hook run after Emacspeak is started.")

(custom-autoload 'emacspeak-startup-hook "emacspeak-setup" t)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-setup" '("emacspeak-")))

;;;***

;;;### (autoloads nil "emacspeak-shx" "emacspeak-shx.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-shx.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-shx" '("shx-cmd-")))

;;;***

;;;### (autoloads nil "emacspeak-solitaire" "emacspeak-solitaire.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-solitaire.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-solitaire" '("emacspeak-solitaire-")))

;;;***

;;;### (autoloads nil "emacspeak-sounds" "emacspeak-sounds.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-sounds.el

(autoload 'emacspeak-audio-setup "emacspeak-sounds" "\
Call appropriate audio environment set command.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-sounds-define-theme "emacspeak-sounds" "\
Define a sounds theme for auditory icons. 

\(fn THEME-NAME FILE-EXT)" nil nil)

(defvar emacspeak-sounds-default-theme (expand-file-name "pan-chimes/" emacspeak-sounds-directory) "\
Default theme for auditory icons. ")

(custom-autoload 'emacspeak-sounds-default-theme "emacspeak-sounds" t)

(defvar emacspeak-play-program (cond ((getenv "EMACSPEAK_PLAY_PROGRAM") (getenv "EMACSPEAK_PLAY_PROGRAM")) ((file-exists-p "/usr/bin/aplay") "/usr/bin/aplay") ((file-exists-p "/usr/bin/play") "/usr/bin/play") ((file-exists-p "/usr/bin/audioplay") "/usr/bin/audioplay") ((file-exists-p "/usr/demo/SOUND/play") "/usr/demo/SOUND/play") (t (expand-file-name emacspeak-etc-directory "play"))) "\
Name of executable that plays sound files. ")

(custom-autoload 'emacspeak-play-program "emacspeak-sounds" t)

(autoload 'emacspeak-sounds-select-theme "emacspeak-sounds" "\
Select theme for auditory icons.

\(fn THEME)" t nil)

(autoload 'emacspeak-queue-auditory-icon "emacspeak-sounds" "\
Queue auditory icon SOUND-NAME.

\(fn SOUND-NAME)" nil nil)

(autoload 'emacspeak-native-auditory-icon "emacspeak-sounds" "\
Play auditory icon using native Emacs player.

\(fn SOUND-NAME)" nil nil)

(autoload 'emacspeak-serve-auditory-icon "emacspeak-sounds" "\
Serve auditory icon SOUND-NAME.

\(fn SOUND-NAME)" nil nil)

(defvar emacspeak-play-args "-q" "\
Set this to nil if using paplay from pulseaudio.")

(custom-autoload 'emacspeak-play-args "emacspeak-sounds" t)

(defvar emacspeak-sox (executable-find "sox") "\
Name of SoX executable.")

(defvar emacspeak-soxplay-command (when (executable-find "play") (format "%s -v 1.2 %%s  earwax &" (executable-find "play"))) "\
Name of play executable from SoX")

(custom-autoload 'emacspeak-soxplay-command "emacspeak-sounds" t)

(autoload 'emacspeak-auditory-icon "emacspeak-sounds" "\
Play an auditory ICON.

\(fn ICON)" nil nil)

(autoload 'emacspeak-toggle-auditory-icons "emacspeak-sounds" "\
Toggle use of auditory icons.
Optional interactive PREFIX arg toggles global value.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-set-auditory-icon-player "emacspeak-sounds" "\
Select  player used for producing auditory icons.
Recommended choices:

emacspeak-serve-auditory-icon for  the wave device.
emacspeak-queue-auditory-icon when using software TTS.

\(fn PLAYER)" t nil)

(autoload 'emacspeak-sounds-reset-sound "emacspeak-sounds" "\
Reload sound drivers.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-sounds" '("emacspeak-")))

;;;***

;;;### (autoloads nil "emacspeak-speak" "emacspeak-speak.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-speak.el

(defvar emacspeak-line-echo nil "\
If t, then emacspeak echoes lines as you type.
You can use \\[emacspeak-toggle-line-echo] to set this
option.")

(custom-autoload 'emacspeak-line-echo "emacspeak-speak" t)

(defvar emacspeak-word-echo t "\
If t, then emacspeak echoes words as you type.
You can use \\[emacspeak-toggle-word-echo] to toggle this
option.")

(custom-autoload 'emacspeak-word-echo "emacspeak-speak" t)

(defvar emacspeak-character-echo t "\
If t, then emacspeak echoes characters  as you type.
You can
use \\[emacspeak-toggle-character-echo] to toggle this
setting.")

(custom-autoload 'emacspeak-character-echo "emacspeak-speak" t)

(autoload 'emacspeak-speak-run-shell-command "emacspeak-speak" "\
Invoke shell COMMAND and display its output as a table. The
results are placed in a buffer in Emacspeak's table browsing
mode. Optional interactive prefix arg read-as-csv interprets the
result as csv. . Use this for running shell commands that produce
tabulated output. This command should be used for shell commands
that produce tabulated output that works with Emacspeak's table
recognizer. Verify this first by running the command in a shell
and executing command `emacspeak-table-display-table-in-region'
normally bound to \\[emacspeak-table-display-table-in-region].

\(fn COMMAND &optional READ-AS-CSV)" t nil)

(autoload 'emacspeak-speak-set-mode-punctuations "emacspeak-speak" "\
Set punctuation mode for all buffers in current mode.

\(fn SETTING)" t nil)

(defvar emacspeak-speak-embedded-url-pattern "<https?:[^ \11]*>" "\
Pattern to recognize embedded URLs.")

(custom-autoload 'emacspeak-speak-embedded-url-pattern "emacspeak-speak" t)

(ems-generate-switcher 'emacspeak-toggle-audio-indentation 'emacspeak-audio-indentation "Toggle state of  Emacspeak  audio indentation.\nInteractive PREFIX arg means toggle  the global default value, and then set the\ncurrent local  value to the result.\nSpecifying the method of indentation as `tones'\nresults in the Dectalk producing a tone whose length is a function of the\nline's indentation.  Specifying `speak'\nresults in the number of initial spaces being spoken.")

(autoload 'emacspeak-speak-line "emacspeak-speak" "\
Speaks current line.  With prefix ARG, speaks the rest of the line
from point.  Negative prefix optional arg speaks from start of line to
point.  Voicifies if option `voice-lock-mode' is on.  Indicates
indentation with a tone or a spoken message if audio indentation is in
use see `emacspeak-toggle-audio-indentation' bound to
\\[emacspeak-toggle-audio-indentation].  Indicates position of point
with an aural highlight if option `emacspeak-show-point' is turned on
--see command `emacspeak-toggle-show-point' bound to
\\[emacspeak-toggle-show-point].  Lines that start hidden blocks of text,
e.g.  outline header lines, or header lines of blocks created by
command `emacspeak-hide-or-expose-block' are indicated with auditory
icon ellipses. Presence of additional presentational overlays (created
via property display, before-string, or after-string) is indicated
with auditory icon `more'.  These can then be spoken using command
\\[emacspeak-speak-overlay-properties].

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-visual-line "emacspeak-speak" "\
Speaks current visual line.
Cues the start of a physical line with auditory icon `left'.

\(fn)" t nil)

(autoload 'emacspeak-speak-spell-current-word "emacspeak-speak" "\
Spell word at  point.

\(fn)" t nil)

(autoload 'emacspeak-speak-word "emacspeak-speak" "\
Speak current word.
With prefix ARG, speaks the rest of the word from point.
Negative prefix arg speaks from start of word to point.
If executed  on the same buffer position a second time, the word is
spelled out  instead of being spoken.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-char "emacspeak-speak" "\
Speak character under point.
Pronounces character phonetically unless  called with a PREFIX arg.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-speak-preceding-char "emacspeak-speak" "\
Speak character before point.

\(fn)" t nil)

(autoload 'emacspeak-speak-char-name "emacspeak-speak" "\
tell me what this is

\(fn CHAR)" t nil)

(autoload 'emacspeak-speak-display-char "emacspeak-speak" "\
Display char under point using current speech display table.
Behavior is the same as command `emacspeak-speak-char'
bound to \\[emacspeak-speak-char]
for characters in the range 0--127.
Optional argument PREFIX  specifies that the character should be spoken phonetically.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-speak-set-display-table "emacspeak-speak" "\
Sets up buffer specific speech display table that controls how
special characters are spoken. Interactive prefix argument causes
setting to be global.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-speak-sentence "emacspeak-speak" "\
Speak current sentence.
With prefix ARG, speaks the rest of the sentence  from point.
Negative prefix arg speaks from start of sentence to point.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-sexp "emacspeak-speak" "\
Speak current sexp.
With prefix ARG, speaks the rest of the sexp  from point.
Negative prefix arg speaks from start of sexp to point. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-page "emacspeak-speak" "\
Speak a page.
With prefix ARG, speaks rest of current page.
Negative prefix arg will read from start of current page to point.
If option  `voice-lock-mode' is on, then it will use any defined personality.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-paragraph "emacspeak-speak" "\
Speak paragraph.
With prefix arg, speaks rest of current paragraph.
Negative prefix arg will read from start of current paragraph to point.
If voice-lock-mode is on, then it will use any defined personality. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-buffer "emacspeak-speak" "\
Speak current buffer  contents.
With prefix ARG, speaks the rest of the buffer from point.
Negative prefix arg speaks from start of buffer to point.
 If voice lock mode is on, the paragraphs in the buffer are
voice annotated first,  see command `emacspeak-speak-voice-annotate-paragraphs'.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-other-buffer "emacspeak-speak" "\
Speak specified buffer.
Useful to listen to a buffer without switching  contexts.

\(fn BUFFER)" t nil)

(autoload 'emacspeak-speak-front-of-buffer "emacspeak-speak" "\
Speak   the buffer from start to   point

\(fn)" t nil)

(autoload 'emacspeak-speak-rest-of-buffer "emacspeak-speak" "\
Speak remainder of the buffer starting at point

\(fn)" t nil)

(autoload 'emacspeak-speak-help "emacspeak-speak" "\
Speak help buffer if one present.
With prefix arg, speaks the rest of the buffer from point.
Negative prefix arg speaks from start of buffer to point.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-minibuffer "emacspeak-speak" "\
Speak the minibuffer contents
 With prefix arg, speaks the rest of the buffer from point.
Negative prefix arg speaks from start of buffer to point.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-get-current-completion "emacspeak-speak" "\
Return the completion string under point in the *Completions* buffer.

\(fn)" nil nil)

(autoload 'emacspeak-speak-minor-mode-line "emacspeak-speak" "\
Speak the minor mode-information.
Optional  interactive prefix arg `copy-as-kill' copies spoken info to kill ring.

\(fn &optional COPY-AS-KILL)" t nil)

(autoload 'emacspeak-speak-buffer-filename "emacspeak-speak" "\
Speak name of file being visited in current buffer.
Speak default directory if invoked in a dired buffer,
or when the buffer is not visiting any file.
Interactive prefix arg `filename' speaks only the final path
component.
The result is put in the kill ring for convenience.

\(fn &optional FILENAME)" t nil)

(defvar emacspeak-use-header-line t "\
Use default header line defined  by Emacspeak for buffers that
dont customize the header.")

(custom-autoload 'emacspeak-use-header-line "emacspeak-speak" t)

(autoload 'emacspeak-toggle-header-line "emacspeak-speak" "\
Toggle Emacspeak's default header line.

\(fn)" t nil)

(autoload 'emacspeak-read-previous-line "emacspeak-speak" "\
Read previous line, specified by an offset, without moving.
Default is to read the previous line. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-read-next-line "emacspeak-speak" "\
Read next line, specified by an offset, without moving.
Default is to read the next line. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-read-previous-word "emacspeak-speak" "\
Read previous word, specified as a prefix arg, without moving.
Default is to read the previous word. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-read-next-word "emacspeak-speak" "\
Read next word, specified as a numeric  arg, without moving.
Default is to read the next word. 

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-world-clock "emacspeak-speak" "\
Display current date and time  for specified zone.
Optional second arg `set' sets the TZ environment variable as well.

\(fn ZONE &optional SET)" t nil)

(autoload 'emacspeak-speak-time "emacspeak-speak" "\
Speak the time.
Optional interactive prefix arg `C-u'invokes world clock.
Timezone is specified using minibuffer completion.
Second interactive prefix sets clock to new timezone.

\(fn &optional WORLD)" t nil)

(autoload 'emacspeak-speak-seconds-since-epoch "emacspeak-speak" "\
Speaks time value specified as seconds  since epoch, e.g. as from float-time.

\(fn SECONDS)" t nil)

(autoload 'emacspeak-speak-microseconds-since-epoch "emacspeak-speak" "\
Speaks time value specified as microseconds  since epoch, e.g. as from float-time.

\(fn MS)" t nil)

(autoload 'emacspeak-speak-milliseconds-since-epoch "emacspeak-speak" "\
Speaks time value specified as milliseconds  since epoch, e.g. as from float-time.

\(fn MS)" t nil)

(autoload 'emacspeak-speak-date-as-seconds "emacspeak-speak" "\
Read time value as a human-readable string, return seconds.
Seconds value is also placed in the kill-ring.

\(fn TIME)" t nil)

(autoload 'emacspeak-speak-version "emacspeak-speak" "\
Announce version information for running emacspeak.
Optional interactive prefix arg `speak-rev' speaks only the Git revision number.

\(fn &optional SPEAK-REV)" t nil)

(autoload 'emacspeak-speak-current-kill "emacspeak-speak" "\
Speak the current kill entry.
This is the text that will be yanked in
by the next \\[yank]. Prefix numeric arg, COUNT, specifies that the
text that will be yanked as a result of a \\[yank] followed by count-1
\\[yank-pop] be spoken. The kill number that is spoken says what
numeric prefix arg to give to command yank.

\(fn &optional COUNT)" t nil)

(autoload 'emacspeak-zap-tts "emacspeak-speak" "\
Send this command to the TTS directly.

\(fn)" t nil)

(autoload 'emacspeak-dial-dtk "emacspeak-speak" "\
Prompt for and dial a phone NUMBER with the Dectalk.

\(fn NUMBER)" t nil)

(autoload 'emacspeak-speak-current-mark "emacspeak-speak" "\
Speak the line containing the mark.
With no argument, speaks the line containing the mark--this is
where `exchange-point-and-mark' \\[exchange-point-and-mark] would
jump.  Numeric prefix arg 'COUNT' speaks line containing mark 'n'
where 'n' is one less than the number of times one has to jump
using `set-mark-command' to get to this marked position.  The
location of the mark is indicated by an aural highlight achieved
by a change in voice personality.

\(fn COUNT)" t nil)

(autoload 'emacspeak-speak-this-personality-chunk "emacspeak-speak" "\
Speak chunk of text around point that has current
personality.

\(fn)" t nil)

(autoload 'emacspeak-speak-next-personality-chunk "emacspeak-speak" "\
Moves to the front of next chunk having current personality.
Speak that chunk after moving.

\(fn)" t nil)

(autoload 'emacspeak-speak-previous-personality-chunk "emacspeak-speak" "\
Moves to the front of previous chunk having current personality.
Speak that chunk after moving.

\(fn)" t nil)

(autoload 'emacspeak-speak-this-face-chunk "emacspeak-speak" "\
Speak chunk of text around point that has current face.

\(fn)" t nil)

(autoload 'emacspeak-speak-next-face-chunk "emacspeak-speak" "\
Moves to the front of next chunk having current face.
Speak that chunk after moving.

\(fn)" t nil)

(autoload 'emacspeak-speak-previous-face-chunk "emacspeak-speak" "\
Moves to the front of previous chunk having current face.
Speak that chunk after moving.

\(fn)" t nil)

(autoload 'emacspeak-execute-repeatedly "emacspeak-speak" "\
Execute COMMAND repeatedly.

\(fn COMMAND)" t nil)

(autoload 'emacspeak-speak-continuously "emacspeak-speak" "\
Speak a buffer continuously.
First prompts using the minibuffer for the kind of action to
perform after speaking each chunk.  E.G.  speak a line at a time
etc.  Speaking commences at current buffer position.  Pressing
\\[keyboard-quit] breaks out, leaving point on last chunk that
was spoken.  Any other key continues to speak the buffer.

\(fn)" t nil)

(autoload 'emacspeak-speak-browse-buffer "emacspeak-speak" "\
Browse current buffer.
Default is to speak chunk having current personality.
Interactive prefix arg `browse'  repeatedly browses  through
  chunks having same personality as the current text chunk.

\(fn &optional BROWSE)" t nil)

(autoload 'emacspeak-speak-skim-paragraph "emacspeak-speak" "\
Skim paragraph.
Skimming a paragraph results in the speech speeding up after
the first clause.
Speech is scaled by the value of dtk-speak-skim-scale

\(fn)" t nil)

(autoload 'emacspeak-speak-skim-next-paragraph "emacspeak-speak" "\
Skim next paragraph.

\(fn)" t nil)

(autoload 'emacspeak-speak-skim-buffer "emacspeak-speak" "\
Skim the current buffer  a paragraph at a time.

\(fn)" t nil)

(autoload 'emacspeak-completion-pick-completion "emacspeak-speak" "\
Pick completion and return safely where we came from.

\(fn)" t nil)

(autoload 'emacspeak-toggle-inaudible-or-comint-autospeak "emacspeak-speak" "\
Toggle comint-autospeak when in a comint buffer.
Otherwise call voice-setup-toggle-silence-personality which toggles the
personality under point.

\(fn)" t nil)

(ems-generate-switcher 'emacspeak-toggle-comint-output-monitor 'emacspeak-comint-output-monitor "Toggle state of Emacspeak comint monitor.\nWhen turned on, comint output is automatically spoken.  Turn this on if\nyou want your shell to speak its results.  Interactive\nPREFIX arg means toggle the global default value, and then\nset the current local value to the result.")

(autoload 'emacspeak-speak-previous-field "emacspeak-speak" "\
Move to previous field and speak it.

\(fn)" t nil)

(autoload 'emacspeak-speak-message-again "emacspeak-speak" "\
Speak the last message from Emacs once again.
The message is also placed in the kill ring for convenient yanking
if `emacspeak-speak-message-again-should-copy-to-kill-ring' is set.

\(fn &optional FROM-MESSAGE-CACHE)" t nil)

(autoload 'emacspeak-speak-window-information "emacspeak-speak" "\
Speaks information about current window.

\(fn)" t nil)

(autoload 'emacspeak-speak-current-window "emacspeak-speak" "\
Speak contents of current window.
Speaks entire window irrespective of point.

\(fn)" t nil)

(autoload 'emacspeak-speak-other-window "emacspeak-speak" "\
Speak contents of `other' window.
Speaks entire window irrespective of point.
Semantics  of `other' is the same as for the builtin Emacs command
`other-window'.
Optional argument ARG  specifies `other' window to speak.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-next-window "emacspeak-speak" "\
Speak the next window.

\(fn)" t nil)

(autoload 'emacspeak-speak-previous-window "emacspeak-speak" "\
Speak the previous window.

\(fn)" t nil)

(autoload 'emacspeak-owindow-scroll-up "emacspeak-speak" "\
Scroll up the window that command `other-window' would move to.
Speak the window contents after scrolling.

\(fn)" t nil)

(autoload 'emacspeak-owindow-scroll-down "emacspeak-speak" "\
Scroll down  the window that command `other-window' would move to.
Speak the window contents after scrolling.

\(fn)" t nil)

(autoload 'emacspeak-owindow-next-line "emacspeak-speak" "\
Move to the next line in the other window and speak it.
Numeric prefix arg COUNT can specify number of lines to move.

\(fn COUNT)" t nil)

(autoload 'emacspeak-owindow-previous-line "emacspeak-speak" "\
Move to the next line in the other window and speak it.
Numeric prefix arg COUNT specifies number of lines to move.

\(fn COUNT)" t nil)

(autoload 'emacspeak-owindow-speak-line "emacspeak-speak" "\
Speak the current line in the other window.

\(fn)" t nil)

(autoload 'emacspeak-speak-predefined-window "emacspeak-speak" "\
Speak one of the first 10 windows on the screen.
Speaks entire window irrespective of point.
In general, you'll never have Emacs split the screen into more than
two or three.
Argument ARG determines the 'other' window to speak.
Semantics  of `other' is the same as for the builtin Emacs command
`other-window'.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-buffer-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire buffer.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire buffer.

\(fn)" t nil)

(autoload 'emacspeak-speak-help-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire help.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire help.

\(fn)" t nil)

(autoload 'emacspeak-speak-line-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire line.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire line.

\(fn)" t nil)

(autoload 'emacspeak-speak-paragraph-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire paragraph.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire paragraph.

\(fn)" t nil)

(autoload 'emacspeak-speak-page-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire page.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire page.

\(fn)" t nil)

(autoload 'emacspeak-speak-word-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire word.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire word.

\(fn)" t nil)

(autoload 'emacspeak-speak-sexp-interactively "emacspeak-speak" "\
Speak the start of, rest of, or the entire sexp.
's' to speak the start.
'r' to speak the rest.
any other key to speak entire sexp.

\(fn)" t nil)

(autoload 'emacspeak-speak-rectangle "emacspeak-speak" "\
Speak a rectangle of text.
Rectangle is delimited by point and mark.  When call from a
program, arguments specify the START and END of the rectangle.

\(fn START END)" t nil)

(autoload 'emacspeak-voiceify-rectangle "emacspeak-speak" "\
Voicify the current rectangle.
When calling from a program,arguments are
START END personality
Prompts for PERSONALITY  with completion when called interactively.

\(fn START END &optional PERSONALITY)" t nil)

(autoload 'emacspeak-voiceify-region "emacspeak-speak" "\
Voicify the current region.
When calling from a program,arguments are
START END personality.
Prompts for PERSONALITY  with completion when called interactively.

\(fn START END &optional PERSONALITY)" t nil)

(autoload 'emacspeak-speak-spaces-at-point "emacspeak-speak" "\
Speak the white space at point.

\(fn)" t nil)

(autoload 'emacspeak-switch-to-reference-buffer "emacspeak-speak" "\
Switch back to buffer that generated completions.

\(fn)" t nil)

(autoload 'emacspeak-completions-move-to-completion-group "emacspeak-speak" "\
Move to group of choices beginning with character last
typed. If no such group exists, then we try to search for that
char, or dont move. 

\(fn)" t nil)

(autoload 'emacspeak-mark-backward-mark "emacspeak-speak" "\
Cycle backward through the mark ring.

\(fn)" t nil)

(autoload 'emacspeak-speak-and-skip-extent-upto-char "emacspeak-speak" "\
Search forward from point until we hit char.
Speak text between point and the char we hit.

\(fn CHAR)" t nil)

(autoload 'emacspeak-speak-and-skip-extent-upto-this-char "emacspeak-speak" "\
Speak extent delimited by point and last character typed.

\(fn)" t nil)

(autoload 'emacspeak-speak-message-at-time "emacspeak-speak" "\
Set up ring-at-time to speak message at specified time.
Provides simple stop watch functionality in addition to other things.
See documentation for command run-at-time for details on time-spec.

\(fn TIME MESSAGE)" t nil)

(autoload 'emacspeak-speak-load-directory-settings "emacspeak-speak" "\
Load a directory specific Emacspeak settings file.
This is typically used to load up settings that are specific to
an electronic book consisting of many files in the same
directory.

\(fn &optional DIRECTORY)" t nil)

(defvar emacspeak-silence-hook nil "\
Functions run after emacspeak-silence is called.")

(custom-autoload 'emacspeak-silence-hook "emacspeak-speak" t)

(autoload 'emacspeak-silence "emacspeak-speak" "\
Silence is golden. Stop speech, and pause/resume any media
streams. Runs `emacspeak-silence-hook' which can be used to
configure which media players get silenced or paused/resumed.

\(fn)" t nil)

(autoload 'emacspeak-speak-hostname "emacspeak-speak" "\
Speak host name.

\(fn)" t nil)

(autoload 'emacspeak-speak-show-active-network-interfaces "emacspeak-speak" "\
Shows all active network interfaces in the echo area.
With interactive prefix argument ADDRESS it prompts for a
specific interface and shows its address. The address is
also copied to the kill ring for convenient yanking.

\(fn &optional ADDRESS)" t nil)

(autoload 'emacspeak-launch-application "emacspeak-speak" "\
Launch an application.
This command  is designed for use in a windowing environment like X.

\(fn COMMAND)" t nil)

(autoload 'describe-help-keys "emacspeak-speak" "\
Show bindings under C-h.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-speak" '("emacspeak-" "ems-" "linum-mode" "voice-lock-voiceify-faces")))

;;;***

;;;### (autoloads nil "emacspeak-speedbar" "emacspeak-speedbar.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-speedbar.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-speedbar" '("emacspeak-speedbar-")))

;;;***

;;;### (autoloads nil "emacspeak-sudoku" "emacspeak-sudoku.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-sudoku.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-sudoku" '("emacspeak-sudoku-")))

;;;***

;;;### (autoloads nil "emacspeak-table" "emacspeak-table.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-table.el

(autoload 'emacspeak-table-make-table "emacspeak-table" "\
Construct a table object from elements.

\(fn ELEMENTS)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-table" '("emacspeak-table-")))

;;;***

;;;### (autoloads nil "emacspeak-table-ui" "emacspeak-table-ui.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-table-ui.el

(autoload 'emacspeak-table-find-file "emacspeak-table-ui" "\
Open a file containing table data and display it in table mode.
emacspeak table mode is designed to let you browse tabular data using
all the power of the two-dimensional spatial layout while giving you
sufficient contextual information.  The etc/tables subdirectory of the
emacspeak distribution contains some sample tables --these are the
CalTrain schedules.  Execute command `describe-mode' bound to
\\[describe-mode] in a buffer that is in emacspeak table mode to read
the documentation on the table browser.

\(fn FILENAME)" t nil)

(autoload 'emacspeak-table-find-csv-file "emacspeak-table-ui" "\
Process a csv (comma separated values) file.
The processed  data is presented using emacspeak table navigation. 

\(fn FILENAME)" t nil)

(autoload 'emacspeak-table-view-csv-buffer "emacspeak-table-ui" "\
Process a csv (comma separated values) data.
The processed  data is  presented using emacspeak table navigation. 

\(fn &optional BUFFER-NAME)" t nil)

(autoload 'emacspeak-table-view-csv-url "emacspeak-table-ui" "\
Process a csv (comma separated values) data at  `URL'.
The processed  data is  presented using emacspeak table navigation. 

\(fn URL &optional BUFFER-NAME)" t nil)

(autoload 'emacspeak-table-display-table-in-region "emacspeak-table-ui" "\
Recognize tabular data in current region and display it in table
browsing mode in a a separate buffer.
emacspeak table mode is designed to let you browse tabular data using
all the power of the two-dimensional spatial layout while giving you
sufficient contextual information.  The tables subdirectory of the
emacspeak distribution contains some sample tables --these are the
CalTrain schedules.  Execute command `describe-mode' bound to
\\[describe-mode] in a buffer that is in emacspeak table mode to read
the documentation on the table browser.

\(fn START END)" t nil)

(autoload 'emacspeak-table-next-row "emacspeak-table-ui" "\
Move to the next row if possible

\(fn &optional COUNT)" t nil)

(autoload 'emacspeak-table-previous-row "emacspeak-table-ui" "\
Move to the previous row if possible

\(fn &optional COUNT)" t nil)

(autoload 'emacspeak-table-next-column "emacspeak-table-ui" "\
Move to the next column if possible

\(fn &optional COUNT)" t nil)

(autoload 'emacspeak-table-previous-column "emacspeak-table-ui" "\
Move to the previous column  if possible

\(fn &optional COUNT)" t nil)

(autoload 'emacspeak-table-copy-to-clipboard "emacspeak-table-ui" "\
Copy table in current buffer to the table clipboard.
Current buffer must be in emacspeak-table mode.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-table-ui" '("emacspeak-table-" "ems-csv-")))

;;;***

;;;### (autoloads nil "emacspeak-tabulate" "emacspeak-tabulate.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-tabulate.el

(autoload 'emacspeak-tabulate-region "emacspeak-tabulate" "\
Voicifies the white-space of a table if one found.  Optional interactive prefix
arg mark-fields specifies if the header row information is used to mark fields
in the white-space.

\(fn START END &optional MARK-FIELDS)" t nil)

(autoload 'ems-tabulate-parse-region "emacspeak-tabulate" "\
Parse  region as tabular data and return a vector of vectors

\(fn START END)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-tabulate" '("ems-")))

;;;***

;;;### (autoloads nil "emacspeak-tapestry" "emacspeak-tapestry.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-tapestry.el

(autoload 'emacspeak-tapestry-describe-tapestry "emacspeak-tapestry" "\
Describe the current layout of visible buffers in current frame.
Use interactive prefix arg to get coordinate positions of the
displayed buffers.

\(fn &optional DETAILS)" t nil)

(autoload 'emacspeak-tapestry-select-window-by-name "emacspeak-tapestry" "\
Select window by the name of the buffer it displays.
This is useful when using modes like ECB or the new GDB UI where
  you want to preserve the window layout 
but quickly switch to a window by name.

\(fn BUFFER-NAME)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-tapestry" '("emacspeak-speak-window-layout")))

;;;***

;;;### (autoloads nil "emacspeak-tar" "emacspeak-tar.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-tar.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-tar" '("emacspeak-tar-s")))

;;;***

;;;### (autoloads nil "emacspeak-tcl" "emacspeak-tcl.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-tcl.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-tcl" '("tcl-")))

;;;***

;;;### (autoloads nil "emacspeak-tetris" "emacspeak-tetris.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-tetris.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-tetris" '("emacspeak-tetris-" "tetris-")))

;;;***

;;;### (autoloads nil "emacspeak-texinfo" "emacspeak-texinfo.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-texinfo.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-texinfo" '("emacspeak-texinfo-mode-hook")))

;;;***

;;;### (autoloads nil "emacspeak-threes" "emacspeak-threes.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-threes.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-threes" '("emacspeak-threes-")))

;;;***

;;;### (autoloads nil "emacspeak-todo-mode" "emacspeak-todo-mode.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-todo-mode.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-todo-mode" '("emacspeak-todo-mode-navigation-commands")))

;;;***

;;;### (autoloads nil "emacspeak-twittering" "emacspeak-twittering.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-twittering.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-twittering" '("emacspeak-twittering-")))

;;;***

;;;### (autoloads nil "emacspeak-url-template" "emacspeak-url-template.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-url-template.el

(autoload 'emacspeak-url-template-get "emacspeak-url-template" "\
Lookup key and return corresponding template. 

\(fn KEY)" nil nil)

(autoload 'emacspeak-url-template-define "emacspeak-url-template" "\
Define a URL template.

name Name used to identify template
template Template URI with `%s' for slots
generators List of prompters.
 Generators are strings or functions.
 String values specify prompts.
 Function values are called to obtain values.
post-action Function called to apply post actions.
 Possible actions include speaking the result.
fetcher Unless specified, browse-url retrieves URL.
 If specified, fetcher is a function of one arg
 that is called with the URI to retrieve.
documentation Documents this template resource.
dont-url-encode if true then url arguments are not url-encoded 

\(fn NAME TEMPLATE &optional GENERATORS POST-ACTION DOCUMENTATION FETCHER DONT-URL-ENCODE)" nil nil)

(autoload 'emacspeak-url-template-load "emacspeak-url-template" "\
Load URL template resources from specified location.

\(fn FILE)" t nil)

(autoload 'emacspeak-url-template-open "emacspeak-url-template" "\
Fetch resource identified by URL template.

\(fn UT)" nil nil)

(autoload 'emacspeak-url-template-fetch "emacspeak-url-template" "\
Fetch a pre-defined resource.
Use Emacs completion to obtain a list of available resources.
Resources typically prompt for the relevant information
before completing the request.
Optional interactive prefix arg displays documentation for specified resource.

\(fn &optional DOCUMENTATION)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-url-template" '("emacspeak-url-")))

;;;***

;;;### (autoloads nil "emacspeak-vdiff" "emacspeak-vdiff.el" (0 0
;;;;;;  0 0))
;;; Generated autoloads from emacspeak-vdiff.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-vdiff" '("emacspeak-vdiff-")))

;;;***

;;;### (autoloads nil "emacspeak-view" "emacspeak-view.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-view.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-view" '("emacspeak-view-")))

;;;***

;;;### (autoloads nil "emacspeak-vlc" "emacspeak-vlc.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-vlc.el

(autoload 'emacspeak-vlc "emacspeak-vlc" "\
Start or control Emacspeak VLC player.

Uses current context to prompt for media to play.
Controls media playback when already playing a stream.

\\{emacspeak-vlc-mode-map}.

\(fn)" t nil)

(autoload 'emacspeak-vlc-pop-to-player "emacspeak-vlc" "\
Pop to vlc buffer.

\(fn)" t nil)

(autoload 'emacspeak-vlc-url "emacspeak-vlc" "\
Call emacspeak-vlc with specified URL.

\(fn URL)" t nil)

(autoload 'emacspeak-vlc-player "emacspeak-vlc" "\
Play specified resource using vlc.
Resource is a media resource or playlist containing media resources.
The player is placed in a buffer in emacspeak-vlc-mode.

\(fn RESOURCE)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-vlc" '("emacspeak-vlc-")))

;;;***

;;;### (autoloads nil "emacspeak-vm" "emacspeak-vm.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-vm.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-vm" '("emacspeak-vm-")))

;;;***

;;;### (autoloads nil "emacspeak-we" "emacspeak-we.el" (0 0 0 0))
;;; Generated autoloads from emacspeak-we.el

(autoload 'emacspeak-we-url-rewrite-and-follow "emacspeak-we" "\
Apply a url rewrite rule as specified in the current buffer
before following link under point.  If no rewrite rule is
defined, first prompt for one.  Rewrite rules are of the
form `(from to)' where from and to are strings.  Typically, the
rewrite rule is automatically set up by Emacspeak tools like
websearch where a rewrite rule is known.  Rewrite rules are
useful in jumping directly to the printer friendly version of an
article for example.  Optional interactive prefix arg prompts for
a rewrite rule even if one is already defined.

\(fn &optional PROMPT)" t nil)

(defvar emacspeak-we-xsl-p nil "\
T means we apply XSL before displaying HTML.")

(custom-autoload 'emacspeak-we-xsl-p "emacspeak-we" t)

(defvar emacspeak-we-xsl-params nil "\
XSL params if any to pass to emacspeak-xslt-region.")

(defvar emacspeak-we-cleanup-bogus-quotes t "\
Clean up bogus Unicode chars for magic quotes.")

(custom-autoload 'emacspeak-we-cleanup-bogus-quotes "emacspeak-we" t)

(autoload 'emacspeak-we-xslt-apply "emacspeak-we" "\
Apply specified transformation to current Web page.

\(fn XSL)" t nil)

(autoload 'emacspeak-we-xslt-select "emacspeak-we" "\
Select XSL transformation applied to Web pages before they are displayed .

\(fn XSL)" t nil)

(autoload 'emacspeak-we-xsl-toggle "emacspeak-we" "\
Toggle  application of XSL transformations.

\(fn)" t nil)

(autoload 'emacspeak-we-count-matches "emacspeak-we" "\
Count matches for locator  in Web page.

\(fn URL LOCATOR)" t nil)

(autoload 'emacspeak-we-count-nested-tables "emacspeak-we" "\
Count nested tables in Web page.

\(fn URL)" t nil)

(autoload 'emacspeak-we-count-tables "emacspeak-we" "\
Count  tables in Web page.

\(fn URL)" t nil)

(defvar emacspeak-we-xsl-keep-result nil "\
Toggle via command \\[emacspeak-we-toggle-xsl-keep-result].")

(autoload 'emacspeak-we-toggle-xsl-keep-result "emacspeak-we" "\
Toggle xsl keep result flag.

\(fn)" t nil)

(autoload 'emacspeak-we-xslt-filter "emacspeak-we" "\
Extract elements matching specified XPath path locator
from Web page -- default is the current page being viewed.

\(fn PATH URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-xslt-junk "emacspeak-we" "\
Junk elements matching specified locator.

\(fn PATH URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-media-streams "emacspeak-we" "\
Extract links to media streams.
operate on current web page when in a browser buffer; otherwise
 prompt for url.  Optional arg `speak' specifies if the result
 should be spoken automatically.

\(fn URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-print-streams "emacspeak-we" "\
Extract links to printable  streams.
operate on current web page when in a browser buffer; otherwise
 prompt for url.  Optional arg `speak' specifies if the result
 should be spoken automatically.

\(fn URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-follow-and-extract-main "emacspeak-we" "\
Follow URL, then extract role=main.

\(fn &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-media-streams-under-point "emacspeak-we" "\
In browser buffers, extract media streams from url under point.

\(fn)" t nil)

(autoload 'emacspeak-we-extract-matching-urls "emacspeak-we" "\
Extracts links whose URL matches pattern.

\(fn PATTERN URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-nested-table "emacspeak-we" "\
Extract nested table specified by `table-index'. Default is to
operate on current web page when in a browser buffer; otherwise
prompt for URL. Optional arg `speak' specifies if the result should be
spoken automatically.

\(fn INDEX URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-nested-table-list "emacspeak-we" "\
Extract specified list of tables from a Web page.

\(fn TABLES URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-table-by-position "emacspeak-we" "\
Extract table at specified pos.
Default is to extract from current page.

\(fn POS URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-tables-by-position-list "emacspeak-we" "\
Extract specified list of nested tables from a WWW page.
Tables are specified by their position in the list
 of nested tables found in the page.

\(fn POSITIONS URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-table-by-match "emacspeak-we" "\
Extract table containing  specified match.
 Optional arg url specifies the page to extract content from.

\(fn MATCH URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-tables-by-match-list "emacspeak-we" "\
Extract specified  tables from a WWW page.
Tables are specified by containing  match pattern
 found in the match list.

\(fn MATCH-LIST URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-by-class "emacspeak-we" "\
Extract elements having specified class attribute from HTML. Extracts
specified elements from current WWW page and displays it in a separate
buffer. Interactive use provides list of class values as completion.

\(fn CLASS URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-junk-by-class "emacspeak-we" "\
Extract elements not having specified class attribute from HTML. Extracts
specified elements from current WWW page and displays it in a separate
buffer. Interactive use provides list of class values as completion.

\(fn CLASS URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-by-class-list "emacspeak-we" "\
Extract elements having class specified in list `classes' from HTML.
Extracts specified elements from current WWW page and displays it
in a separate buffer.  Interactive use provides list of class
values as completion. 

\(fn CLASSES URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-junk-by-class-list "emacspeak-we" "\
Extract elements not having class specified in list `classes' from HTML.
Extracts specified elements from current WWW page and displays it
in a separate buffer.  Interactive use provides list of class
values as completion. 

\(fn CLASSES URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-by-id "emacspeak-we" "\
Extract elements having specified id attribute from HTML. Extracts
specified elements from current WWW page and displays it in a separate
buffer.
Interactive use provides list of id values as completion.

\(fn ID URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-by-id-list "emacspeak-we" "\
Extract elements having id specified in list `ids' from HTML.
Extracts specified elements from current WWW page and displays it in a
separate buffer. Interactive use provides list of id values as completion. 

\(fn IDS URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-id-text "emacspeak-we" "\
Extract text nodes from elements having specified id attribute from HTML. Extracts
specified elements from current WWW page and displays it in a separate
buffer.
Interactive use provides list of id values as completion.

\(fn ID URL &optional SPEAK)" t nil)

(autoload 'emacspeak-we-extract-id-list-text "emacspeak-we" "\
Extract text nodes from elements having id specified in list `ids' from HTML.
Extracts specified elements from current WWW page and displays it in a
separate buffer. Interactive use provides list of id values as completion. 

\(fn IDS URL &optional SPEAK)" t nil)

(defvar emacspeak-we-url-rewrite-rule nil "\
URL rewrite rule to use in current buffer.")

(autoload 'emacspeak-we-class-filter-and-follow "emacspeak-we" "\
Follow url and point, and filter the result by specified class.
Class can be set locally for a buffer, and overridden with an
interactive prefix arg. If there is a known rewrite url rule, that is
used as well.

\(fn CLASS URL &optional PROMPT)" t nil)

(autoload 'emacspeak-we-follow-and-filter-by-id "emacspeak-we" "\
Follow url and point, and filter the result by specified id.
Id can be set locally for a buffer, and overridden with an
interactive prefix arg. If there is a known rewrite url rule, that is
used as well.

\(fn ID PROMPT)" t nil)

(autoload 'emacspeak-we-style-filter "emacspeak-we" "\
Extract elements matching specified style
from HTML.  Extracts specified elements from current WWW
page and displays it in a separate buffer.  Optional arg url
specifies the page to extract contents  from.

\(fn STYLE URL &optional SPEAK)" t nil)

(defvar emacspeak-we-recent-xpath-filter "//p" "\
Caches most recently used xpath filter.")

(defvar emacspeak-we-paragraphs-xpath-filter "//p" "\
Filter paragraphs.")

(autoload 'emacspeak-we-xpath-filter-and-follow "emacspeak-we" "\
Follow url and point, and filter the result by specified xpath.
XPath can be set locally for a buffer, and overridden with an
interactive prefix arg. If there is a known rewrite url rule, that is
used as well.

\(fn &optional PROMPT)" t nil)

(autoload 'emacspeak-we-class-filter-and-follow-link "emacspeak-we" "\
Follow url and point, and filter the result by specified class.
Class can be set locally for a buffer, and overridden with an
interactive prefix arg. If there is a known rewrite url rule, that is
used as well.

\(fn &optional PROMPT)" t nil)

(autoload 'emacspeak-we-xpath-junk-and-follow "emacspeak-we" "\
Follow url and point, and filter the result by junking
elements specified by xpath.
XPath can be set locally for a buffer, and overridden with an
interactive prefix arg. If there is a known rewrite url rule, that is
used as well.

\(fn &optional PROMPT)" t nil)

(cl-declaim (special emacspeak-we-xsl-map))

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-we" '("emacspeak-we-")))

;;;***

;;;### (autoloads nil "emacspeak-websearch" "emacspeak-websearch.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-websearch.el

(let ((loads (get 'emacspeak-websearch 'custom-loads))) (if (member '"emacspeak-websearch" loads) nil (put 'emacspeak-websearch 'custom-loads (cons '"emacspeak-websearch" loads))))

(autoload 'emacspeak-websearch-help "emacspeak-websearch" "\
Displays key mapping used by Emacspeak Websearch.

\(fn)" t nil)

(autoload 'emacspeak-websearch-dispatch "emacspeak-websearch" "\
 Press `?' to list available search engines.
When using supported browsers,  this interface attempts to speak the most relevant information on the result page.

\(fn)" t nil)

(autoload 'emacspeak-websearch-biblio-search "emacspeak-websearch" "\
Search Computer Science Bibliographies.

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-citeseer-search "emacspeak-websearch" "\
Perform a CiteSeer search. 

\(fn TERM)" t nil)

(autoload 'emacspeak-websearch-foldoc-search "emacspeak-websearch" "\
Perform a FolDoc search. 

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-company-news "emacspeak-websearch" "\
Perform an company news lookup.
Retrieves company news, research, profile, insider trades,  or upgrades/downgrades.

\(fn TICKER &optional PREFIX)" t nil)

(autoload 'emacspeak-websearch-yahoo-historical-chart "emacspeak-websearch" "\
Look up historical stock data.
Optional second arg as-html processes the results as HTML rather than data.

\(fn TICKER &optional AS-HTML)" t nil)

(autoload 'emacspeak-websearch-sourceforge-search "emacspeak-websearch" "\
Search SourceForge Site. 

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-ctan-search "emacspeak-websearch" "\
Search CTAN Comprehensive TeX Archive Network   Site. 

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-cpan-search "emacspeak-websearch" "\
Search CPAN  Comprehensive Perl Archive Network   Site. 

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-software-search "emacspeak-websearch" "\
Search SourceForge, Freshmeat and other sites. 

\(fn)" t nil)

(autoload 'emacspeak-websearch-gutenberg "emacspeak-websearch" "\
Perform an Gutenberg search

\(fn TYPE QUERY)" t nil)

(defvar emacspeak-websearch-google-use-https t "\
Specify whether we use secure connections for Google search.")

(custom-autoload 'emacspeak-websearch-google-use-https "emacspeak-websearch" t)

(defvar emacspeak-websearch-google-number-of-results 25 "\
Number of results to return from google search.")

(custom-autoload 'emacspeak-websearch-google-number-of-results "emacspeak-websearch" t)

(defvar emacspeak-websearch-google-options nil "\
Additional options to pass to Google e.g. &xx=yy...")

(custom-autoload 'emacspeak-websearch-google-options "emacspeak-websearch" t)

(autoload 'emacspeak-websearch-google "emacspeak-websearch" "\
Perform a Google search.  First optional interactive prefix arg
`flag' prompts for additional search options. Second interactive
prefix arg is equivalent to hitting the I'm Feeling Lucky button on Google. 

\(fn QUERY &optional FLAG)" t nil)

(autoload 'emacspeak-websearch-google-mobile "emacspeak-websearch" "\
Perform a Google Mobile search.  First optional interactive prefix arg
`flag' prompts for additional search options. Second interactive
prefix arg is equivalent to hitting the I'm Feeling Lucky button on Google. 

\(fn QUERY &optional FLAG)" t nil)

(autoload 'emacspeak-websearch-accessible-google "emacspeak-websearch" "\
Use Google Lite (Experimental).
Optional prefix arg prompts for toolbelt options.

\(fn QUERY &optional OPTIONS)" t nil)

(autoload 'emacspeak-websearch-google-with-toolbelt "emacspeak-websearch" "\
Launch Google search with toolbelt.

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-google-feeling-lucky "emacspeak-websearch" "\
Do a I'm Feeling Lucky Google search.

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-google-specialize "emacspeak-websearch" "\
Perform a specialized Google search. See the Google site for
  what is possible here:
https://www.google.com/options/specialsearches.html 

\(fn SPECIALIZE QUERY)" t nil)

(autoload 'emacspeak-websearch-google-search-in-date-range "emacspeak-websearch" "\
Use this from inside the calendar to do Google date-range searches.

\(fn)" t nil)

(autoload 'emacspeak-websearch-google-news "emacspeak-websearch" "\
Invoke Google News url template.

\(fn)" t nil)

(autoload 'emacspeak-websearch-google-category-news "emacspeak-websearch" "\
Browse Google News by category.

\(fn)" t nil)

(autoload 'emacspeak-websearch-google-regional-news "emacspeak-websearch" "\
Browse Google News by region.

\(fn)" t nil)

(autoload 'emacspeak-websearch-ask-jeeves "emacspeak-websearch" "\
Ask Jeeves for the answer.

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-news-yahoo "emacspeak-websearch" "\
Perform an Yahoo News search.
Optional prefix arg  avoids scraping  information from HTML.

\(fn QUERY &optional RSS)" t nil)

(autoload 'emacspeak-websearch-open-directory-search "emacspeak-websearch" "\
Perform an Open Directory search

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-merriam-webster-search "emacspeak-websearch" "\
Search the Merriam Webster Dictionary.

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-weather "emacspeak-websearch" "\
Get weather forecast for specified zip code.

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-wikipedia-search "emacspeak-websearch" "\
Search Wikipedia using Google.

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-yahoo "emacspeak-websearch" "\
Perform an Yahoo  search

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-exchange-rate-converter "emacspeak-websearch" "\
Currency converter.

\(fn CONVERSION-SPEC)" t nil)

(autoload 'emacspeak-websearch-yahoo-exchange-rate-converter "emacspeak-websearch" "\
Currency converter.

\(fn CONVERSION-SPEC)" t nil)

(autoload 'emacspeak-websearch-youtube-search "emacspeak-websearch" "\
YouTube search.

\(fn QUERY)" t nil)

(autoload 'emacspeak-websearch-amazon-search "emacspeak-websearch" "\
Amazon search.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-websearch" '("emacspeak-websearch-")))

;;;***

;;;### (autoloads nil "emacspeak-webspace" "emacspeak-webspace.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-webspace.el

(autoload 'emacspeak-webspace-mode "emacspeak-webspace" "\
Major mode for Webspace interaction.


\\{emacspeak-webspace-mode-map}

\(fn)" t nil)

(autoload 'emacspeak-webspace-transcode "emacspeak-webspace" "\
Transcode headline at point by following its link property.

\(fn)" t nil)

(autoload 'emacspeak-webspace-yank-link "emacspeak-webspace" "\
Yank link under point into kill ring.

\(fn)" t nil)

(autoload 'emacspeak-webspace-open "emacspeak-webspace" "\
Open headline at point by following its link property.

\(fn)" t nil)

(autoload 'emacspeak-webspace-filter "emacspeak-webspace" "\
Open headline at point by following its link property and filter for content.

\(fn)" t nil)

(define-prefix-command 'emacspeak-webspace 'emacspeak-webspace-keymap)

(autoload 'emacspeak-webspace-headlines "emacspeak-webspace" "\
Startup Headlines ticker using RSS/Atom  feeds.

\(fn)" t nil)

(autoload 'emacspeak-webspace-feed-reader "emacspeak-webspace" "\
Display Feed Reader Feed list in a WebSpace buffer.
Optional interactive prefix arg forces a refresh.

\(fn &optional REFRESH)" t nil)

(autoload 'emacspeak-webspace-knowledge-search "emacspeak-webspace" "\
Perform a Google Knowledge Graph search.
Optional interactive prefix arg `limit' prompts for number of results, default is 1.

\(fn QUERY &optional LIMIT)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-webspace" '("emacspeak-webspace-")))

;;;***

;;;### (autoloads nil "emacspeak-webutils" "emacspeak-webutils.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-webutils.el

(autoload 'emacspeak-webutils-html-string "emacspeak-webutils" "\
Return formatted string.

\(fn HTML-STRING)" nil nil)

(defvar emacspeak-web-pre-process-hook nil "\
Pre-process hook -- to be used for XSL preprocessing etc.")

(defvar emacspeak-web-post-process-hook nil "\
Set locally to a  site specific post processor.
Note that the Web browser should reset this hook after using it.")

(defvar emacspeak-webutils-charent-alist '(("&lt;" . "<") ("&gt;" . ">") ("&quot;" . "\"") ("&apos;" . "'") ("&amp;" . "&")) "\
Entities to unescape when treating badly escaped XML.")

(custom-autoload 'emacspeak-webutils-charent-alist "emacspeak-webutils" t)

(autoload 'emacspeak-webutils-post-process "emacspeak-webutils" "\
Set up post processing steps on a result page.
LOCATOR is a string to search for in the results page.
SPEAKER is a function to call to speak relevant information.
ARGS specifies additional arguments to SPEAKER if any.

\(fn LOCATOR SPEAKER &rest ARGS)" nil nil)

(autoload 'emacspeak-webutils-google-who-links-to-this-page "emacspeak-webutils" "\
Perform a google search to locate documents that link to the
current page.

\(fn)" t nil)

(autoload 'emacspeak-webutils-google-extract-from-cache "emacspeak-webutils" "\
Extract current  page from the Google cache.
With a prefix argument, extracts url under point.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-webutils-google-on-this-site "emacspeak-webutils" "\
Perform a google search restricted to the current WWW site.

\(fn)" t nil)

(autoload 'emacspeak-webutils-google-similar-to-this-page "emacspeak-webutils" "\
Ask Google to find documents similar to this one.

\(fn URL)" t nil)

(autoload 'emacspeak-webutils-transcode-this-url-via-google "emacspeak-webutils" "\
Transcode specified url via Google.

\(fn URL)" nil nil)

(autoload 'emacspeak-webutils-transcode-via-google "emacspeak-webutils" "\
Transcode URL under point via Google.
 Reverse effect with prefix arg for links on a transcoded page.

\(fn &optional UNTRANSCODE)" t nil)

(autoload 'emacspeak-webutils-transcode-current-url-via-google "emacspeak-webutils" "\
Transcode current URL via Google.
  Reverse effect with prefix arg.

\(fn &optional UNTRANSCODE)" t nil)

(autoload 'emacspeak-webutils-jump-to-title-in-content "emacspeak-webutils" "\
Jumps to the title in web document.
The first time it is called, it jumps to the first
instance  of the title.  Repeated calls jump to further
instances.

\(fn)" t nil)

(autoload 'emacspeak-webutils-play-media-at-point "emacspeak-webutils" "\
Play media url under point.
Optional interactive prefix arg `playlist-p' says to treat the link as a playlist.
 A second interactive prefix arg adds mplayer option -allow-dangerous-playlist-parsing

\(fn &optional PLAYLIST-P)" t nil)

(autoload 'emacspeak-webutils-feed-titles "emacspeak-webutils" "\
Return a list of the form `((title url)...) given an RSS/Atom  feed  URL.

\(fn FEED-URL)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-webutils" '("emacspeak-webutils-")))

;;;***

;;;### (autoloads nil "emacspeak-widget" "emacspeak-widget.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from emacspeak-widget.el

(autoload 'emacspeak-widget-summarize-parent "emacspeak-widget" "\
Summarize parent of widget at point.

\(fn)" t nil)

(autoload 'emacspeak-widget-summarize "emacspeak-widget" "\
Summarize specified widget.

\(fn WIDGET)" nil nil)

(autoload 'emacspeak-widget-default-summarize "emacspeak-widget" "\
Fall back summarizer for all widgets

\(fn WIDGET)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-widget" '("emacspeak-widget-")))

;;;***

;;;### (autoloads nil "emacspeak-wizards" "emacspeak-wizards.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-wizards.el

(autoload 'emacspeak-view-emacspeak-news "emacspeak-wizards" "\
Display emacspeak News for a given version.

\(fn)" t nil)

(autoload 'emacspeak-view-emacspeak-tips "emacspeak-wizards" "\
Browse  Emacspeak productivity tips.

\(fn)" t nil)

(autoload 'emacspeak-copy-current-file "emacspeak-wizards" "\
Copy file visited in current buffer to new location.
Prompts for the new location and preserves modification time
  when copying.  If location is a directory, the file is copied
  to that directory under its current name ; if location names
  a file in an existing directory, the specified name is
  used.  Asks for confirmation if the copy will result in an
  existing file being overwritten.

\(fn)" t nil)

(autoload 'emacspeak-link-current-file "emacspeak-wizards" "\
Link (hard link) file visited in current buffer to new location.
Prompts for the new location and preserves modification time
  when linking.  If location is a directory, the file is copied
  to that directory under its current name ; if location names
  a file in an existing directory, the specified name is
  used.  Signals an error if target already exists.

\(fn)" t nil)

(autoload 'emacspeak-symlink-current-file "emacspeak-wizards" "\
Link (symbolic link) file visited in current buffer to new location.
Prompts for the new location and preserves modification time
  when linking.  If location is a directory, the file is copied
  to that directory under its current name ; if location names
  a file in an existing directory, the specified name is
  used.  Signals an error if target already exists.

\(fn)" t nil)

(autoload 'emacspeak-speak-popup-messages "emacspeak-wizards" "\
Pop up messages buffer.
If it is already selected then hide it and try to restore
previous window configuration.

\(fn)" t nil)

(autoload 'emacspeak-wizards-byte-compile-current-buffer "emacspeak-wizards" "\
byte compile current buffer

\(fn)" t nil)

(autoload 'emacspeak-wizards-load-current-file "emacspeak-wizards" "\
load file into emacs

\(fn)" t nil)

(autoload 'emacspeak-wizards-end-of-word "emacspeak-wizards" "\
move to end of word

\(fn ARG)" t nil)

(autoload 'emacspeak-wizards-comma-at-end-of-word "emacspeak-wizards" "\
Move to the end of current word and add a comma.

\(fn)" t nil)

(autoload 'emacspeak-wizards-lacheck-buffer-file "emacspeak-wizards" "\
Run Lacheck on current buffer.

\(fn)" t nil)

(autoload 'emacspeak-wizards-tex-tie-current-word "emacspeak-wizards" "\
Tie the next n  words.

\(fn N)" t nil)

(autoload 'emacspeak-speak-telephone-directory "emacspeak-wizards" "\
Lookup and display a phone number.
With prefix arg, opens the phone book for editing.

\(fn &optional EDIT)" t nil)

(autoload 'emacspeak-wizards-find-file-as-root "emacspeak-wizards" "\
Like `ido-find-file, but automatically edit the file with
root-privileges (using tramp/sudo), if the file is not writable by
user.

\(fn FILE)" t nil)

(autoload 'emacspeak-wizards-vi-as-su-file "emacspeak-wizards" "\
Launch sudo vi on specified file in a terminal.

\(fn FILE)" t nil)

(autoload 'emacspeak-wizards-move-and-speak "emacspeak-wizards" "\
Speaks a chunk of text bounded by point and a target position.
Target position is specified using a navigation command and a
count that specifies how many times to execute that command
first.  Point is left at the target position.  Interactively,
command is specified by pressing the key that invokes the
command.

\(fn COMMAND COUNT)" t nil)

(autoload 'emacspeak-learn-emacs-mode "emacspeak-wizards" "\
Helps you learn the keys.  You can press keys and hear what they do.
To leave, press \\[keyboard-quit].

\(fn)" t nil)

(autoload 'emacspeak-frame-label-or-switch-to-labelled-frame "emacspeak-wizards" "\
Switch to labelled frame.
With optional PREFIX argument, label current frame.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-next-frame-or-buffer "emacspeak-wizards" "\
Move to next buffer.
With optional interactive prefix arg `frame', move to next frame instead.

\(fn &optional FRAME)" t nil)

(autoload 'emacspeak-previous-frame-or-buffer "emacspeak-wizards" "\
Move to previous buffer.
With optional interactive prefix arg `frame', move to previous frame instead.

\(fn &optional FRAME)" t nil)

(autoload 'emacspeak-speak-this-buffer-other-window-display "emacspeak-wizards" "\
Speak this buffer as displayed in a different frame.  Emacs
allows you to display the same buffer in multiple windows or
frames.  These different windows can display different
portions of the buffer.  This is equivalent to leaving a
book open at places at once.  This command allows you to
listen to the places where you have left the book open.  The
number used to invoke this command specifies which of the
displays you wish to speak.  Typically you will have two or
at most three such displays open.  The current display is 0,
the next is 1, and so on.  Optional argument ARG specifies
the display to speak.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-speak-this-buffer-previous-display "emacspeak-wizards" "\
Speak this buffer as displayed in a `previous' window.
See documentation for command
`emacspeak-speak-this-buffer-other-window-display' for the
meaning of `previous'.

\(fn)" t nil)

(autoload 'emacspeak-speak-this-buffer-next-display "emacspeak-wizards" "\
Speak this buffer as displayed in a `previous' window.
See documentation for command
`emacspeak-speak-this-buffer-other-window-display' for the
meaning of `next'.

\(fn)" t nil)

(autoload 'emacspeak-select-this-buffer-other-window-display "emacspeak-wizards" "\
Switch  to this buffer as displayed in a different frame.  Emacs
allows you to display the same buffer in multiple windows or
frames.  These different windows can display different
portions of the buffer.  This is equivalent to leaving a
book open at multiple places at once.  This command allows you to
move to the places where you have left the book open.  The
number used to invoke this command specifies which of the
displays you wish to select.  Typically you will have two or
at most three such displays open.  The current display is 0,
the next is 1, and so on.  Optional argument ARG specifies
the display to select.

\(fn &optional ARG)" t nil)

(autoload 'emacspeak-select-this-buffer-previous-display "emacspeak-wizards" "\
Select this buffer as displayed in a `previous' window.
See documentation for command
`emacspeak-select-this-buffer-other-window-display' for the
meaning of `previous'.

\(fn)" t nil)

(autoload 'emacspeak-select-this-buffer-next-display "emacspeak-wizards" "\
Select this buffer as displayed in a `next' frame.
See documentation for command
`emacspeak-select-this-buffer-other-window-display' for the
meaning of `next'.

\(fn)" t nil)

(autoload 'emacspeak-clipboard-copy "emacspeak-wizards" "\
Copy contents of the region to the emacspeak clipboard. Previous
contents of the clipboard will be overwritten. The Emacspeak clipboard
is a convenient way of sharing information between independent
Emacspeak sessions running on the same or different machines. Do not
use this for sharing information within an Emacs session --Emacs'
register commands are far more efficient and light-weight. Optional
interactive prefix arg results in Emacspeak prompting for the
clipboard file to use. Argument START and END specifies
region. Optional argument PROMPT specifies whether we prompt for the
name of a clipboard file.

\(fn START END &optional PROMPT)" t nil)

(autoload 'emacspeak-clipboard-paste "emacspeak-wizards" "\
Yank contents of the Emacspeak clipboard at point.
The Emacspeak clipboard is a convenient way of sharing information between
independent Emacspeak sessions running on the same or different
machines.  Do not use this for sharing information within an Emacs
session --Emacs' register commands are far more efficient and
light-weight.  Optional interactive prefix arg pastes from
the emacspeak table clipboard instead.

\(fn &optional PASTE-TABLE)" t nil)

(autoload 'emacspeak-wizards-show-eval-result "emacspeak-wizards" "\
Convenience command to pretty-print and view Lisp evaluation results.

\(fn FORM)" t nil)

(autoload 'emacspeak-wizards-show-memory-used "emacspeak-wizards" "\
Convenience command to view state of memory used in this session so far.

\(fn)" t nil)

(autoload 'emacspeak-emergency-tts-restart "emacspeak-wizards" "\
For use in an emergency.
Will start TTS engine specified by
emacspeak-emergency-tts-server.

\(fn)" t nil)

(autoload 'emacspeak-ssh-tts-restart "emacspeak-wizards" "\
Restart specified ssh tts server.

\(fn)" t nil)

(autoload 'emacspeak-show-style-at-point "emacspeak-wizards" "\
Show value of property personality (and possibly face) at point.

\(fn)" t nil)

(autoload 'emacspeak-show-property-at-point "emacspeak-wizards" "\
Show value of PROPERTY at point.
If optional arg property is not supplied, read it interactively.
Provides completion based on properties at point.
If no property is set, show a message and exit.

\(fn &optional PROPERTY)" t nil)

(autoload 'emacspeak-skip-blank-lines-forward "emacspeak-wizards" "\
Move forward across blank lines.
The line under point is then spoken.
Signals end of buffer.

\(fn)" t nil)

(autoload 'emacspeak-skip-blank-lines-backward "emacspeak-wizards" "\
Move backward  across blank lines.
The line under point is   then spoken.
Signals beginning  of buffer.

\(fn)" t nil)

(autoload 'emacspeak-links "emacspeak-wizards" "\
Launch links on  specified URL in a new terminal.

\(fn URL)" t nil)

(autoload 'emacspeak-lynx "emacspeak-wizards" "\
Launch lynx on  specified URL in a new terminal.

\(fn URL)" t nil)

(autoload 'emacspeak-curl "emacspeak-wizards" "\
Grab URL using Curl, and preview it with a browser .

\(fn URL)" t nil)

(autoload 'emacspeak-wizards-terminal "emacspeak-wizards" "\
Launch terminal and rename buffer appropriately.

\(fn PROGRAM)" t nil)

(autoload 'emacspeak-wizards-get-table-content-from-url "emacspeak-wizards" "\
Extract table specified by depth and count from HTML
content at URL.
Extracted content is placed as a csv file in task.csv.

\(fn URL DEPTH COUNT)" t nil)

(autoload 'emacspeak-wizards-get-table-content-from-file "emacspeak-wizards" "\
Extract table specified by depth and count from HTML
content at file.
Extracted content is sent to STDOUT.

\(fn FILE DEPTH COUNT)" t nil)

(autoload 'emacspeak-annotate-add-annotation "emacspeak-wizards" "\
Add annotation to the annotation working buffer.
Prompt for annotation buffer if not already set.
Interactive prefix arg `reset' prompts for the annotation
buffer even if one is already set.
Annotation is entered in a temporary buffer and the
annotation is inserted into the working buffer when complete.

\(fn &optional RESET)" t nil)

(autoload 'emacspeak-wizards-shell-toggle "emacspeak-wizards" "\
Switch to the shell buffer and cd to
 the directory of the current buffer.

\(fn)" t nil)

(autoload 'emacspeak-wizards-rpm-query-in-dired "emacspeak-wizards" "\
Run rpm -qi on current dired entry.

\(fn)" t nil)

(autoload 'emacspeak-wizards-xl-display "emacspeak-wizards" "\
Called to set up preview of an XL file.
Assumes we are in a buffer visiting a .xls file.
Previews those contents as HTML and nukes the buffer
visiting the xls file.

\(fn)" t nil)

(autoload 'emacspeak-wizards-pdf-open "emacspeak-wizards" "\
Open pdf file as text.
Optional interactive prefix arg ask-pwd prompts for password.

\(fn FILENAME &optional ASK-PWD)" t nil)

(autoload 'emacspeak-wizards-ppt-display "emacspeak-wizards" "\
Called to set up preview of an PPT file.
Assumes we are in a buffer visiting a .ppt file.
Previews those contents as HTML and nukes the buffer
visiting the ppt file.

\(fn)" t nil)

(autoload 'emacspeak-wizards-dvi-display "emacspeak-wizards" "\
Called to set up preview of an DVI file.
Assumes we are in a buffer visiting a .DVI file.
Previews those contents as text and nukes the buffer
visiting the DVI file.

\(fn)" t nil)

(autoload 'emacspeak-wizards-generate-finder "emacspeak-wizards" "\
Generate a widget-enabled finder wizard.

\(fn)" t nil)

(autoload 'emacspeak-wizards-finder-find "emacspeak-wizards" "\
Run find-dired on specified switches after prompting for the
directory to where find is to be launched.

\(fn DIRECTORY)" t nil)

(autoload 'emacspeak-customize "emacspeak-wizards" "\
Customize Emacspeak.

\(fn)" t nil)

(autoload 'emacspeak-wizards-show-environment-variable "emacspeak-wizards" "\
Display value of specified environment variable.

\(fn V)" t nil)

(autoload 'emacspeak-wizards-squeeze-blanks "emacspeak-wizards" "\
Squeeze multiple blank lines in current buffer.

\(fn START END)" t nil)

(autoload 'emacspeak-wizards-count-slides-in-region "emacspeak-wizards" "\
Count slides starting from point.

\(fn START END)" t nil)

(autoload 'emacspeak-wizards-how-many-matches "emacspeak-wizards" "\
If you define a file local variable
called `emacspeak-occur-pattern' that holds a regular expression
that matches  lines of interest, you can use this command to conveniently
run `how-many' to count  matching header lines.
With interactive prefix arg, prompts for and remembers the file local pattern.

\(fn START END &optional PREFIX)" t nil)

(autoload 'emacspeak-wizards-occur-header-lines "emacspeak-wizards" "\
If you define a file local variable called
`emacspeak-occur-pattern' that holds a regular expression that
matches header lines, you can use this command to conveniently
run `occur' to find matching header lines. With prefix arg,
prompts for and sets value of the file local pattern.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-kill-buffer-quietly "emacspeak-wizards" "\
Kill current buffer without asking for confirmation.

\(fn)" t nil)

(autoload 'emacspeak-wizards-spot-words "emacspeak-wizards" "\
Searches recursively in all files with extension `ext'
for `word' and displays hits in a compilation buffer.

\(fn EXT WORD)" t nil)

(autoload 'emacspeak-wizards-fix-typo "emacspeak-wizards" "\
Search and replace  recursively in all files with extension `ext'
for `word' and replace it with correction.
Use with caution.

\(fn EXT WORD CORRECTION)" t nil)

(autoload 'emacspeak-wizards-display-pod-as-manpage "emacspeak-wizards" "\
Create a virtual manpage in Emacs from the Perl Online Documentation.

\(fn FILENAME)" t nil)

(autoload 'emacspeak-wizards-fix-read-only-text "emacspeak-wizards" "\
Nuke read-only property on text range.

\(fn START END)" t nil)

(autoload 'emacspeak-wizards-vc-viewer "emacspeak-wizards" "\
View contents of specified virtual console.

\(fn CONSOLE)" t nil)

(autoload 'emacspeak-wizards-vc-viewer-refresh "emacspeak-wizards" "\
Refresh view of VC we're viewing.

\(fn)" t nil)

(autoload 'emacspeak-wizards-vc-n "emacspeak-wizards" "\
Accelerator for VC viewer.

\(fn)" t nil)

(autoload 'emacspeak-wizards-google-transcode "emacspeak-wizards" "\
View Web through Google Transcoder.

\(fn)" t nil)

(autoload 'emacspeak-wizards-find-longest-line-in-region "emacspeak-wizards" "\
Find longest line in region.
Moves to the longest line when called interactively.

\(fn START END)" t nil)

(autoload 'emacspeak-wizards-find-longest-paragraph-in-region "emacspeak-wizards" "\
Find longest paragraph in region.
Moves to the longest paragraph when called interactively.

\(fn START END)" t nil)

(autoload 'emacspeak-wizards-find-grep "emacspeak-wizards" "\
Run compile using find and grep.
Interactive  arguments specify filename pattern and search pattern.

\(fn GLOB PATTERN)" t nil)

(autoload 'emacspeak-wizards-show-face "emacspeak-wizards" "\
Show salient properties of specified face.

\(fn FACE)" t nil)

(autoload 'emacspeak-wizards-voice-sampler "emacspeak-wizards" "\
Read a personality  and apply it to the current line.

\(fn PERSONALITY)" t nil)

(autoload 'emacspeak-wizards-generate-voice-sampler "emacspeak-wizards" "\
Generate a buffer that shows a sample line in all the ACSS settings
for the current voice family.

\(fn STEP)" t nil)

(autoload 'emacspeak-wizards-show-defined-voices "emacspeak-wizards" "\
Display a buffer with sample text in the defined voices.

\(fn)" t nil)

(autoload 'emacspeak-wizards-tramp-open-location "emacspeak-wizards" "\
Open specified tramp location.
Location is specified by name.

\(fn NAME)" t nil)

(autoload 'emacspeak-wizards-speak-iso-datetime "emacspeak-wizards" "\
Make ISO date-time speech friendly.

\(fn ISO)" t nil)

(autoload 'emacspeak-wizards-toggle-mm-dd-yyyy-date-pronouncer "emacspeak-wizards" "\
Toggle pronunciation of mm-dd-yyyy dates.

\(fn)" t nil)

(autoload 'emacspeak-wizards-toggle-yyyymmdd-date-pronouncer "emacspeak-wizards" "\
Toggle pronunciation of yyyymmdd  dates.

\(fn)" t nil)

(autoload 'emacspeak-wizards-units "emacspeak-wizards" "\
Run units in a comint sub-process.

\(fn)" t nil)

(autoload 'emacspeak-wizards-rivo "emacspeak-wizards" "\
Rivo wizard.
Prompts for relevant information and schedules a rivo job using
  UNIX At scheduling facility.
RIVO is implemented by rivo.pl ---
 a Perl script  that can be used to launch streaming media and record
   streaming media for  a specified duration.

\(fn WHEN CHANNEL STOP-TIME OUTPUT DIRECTORY)" t nil)

(autoload 'emacspeak-wizards-shell-bind-keys "emacspeak-wizards" "\
Set up additional shell mode keys.

\(fn)" nil nil)

(autoload 'emacspeak-wizards-next-shell "emacspeak-wizards" "\
Switch to next shell.

\(fn)" t nil)

(autoload 'emacspeak-wizards-previous-shell "emacspeak-wizards" "\
Switch to previous shell.

\(fn)" t nil)

(autoload 'emacspeak-wizards-shell "emacspeak-wizards" "\
Run Emacs built-in `shell' command when not in a shell buffer, or
when called with a prefix argument. When called from a shell buffer,
switches to `next' shell buffer. When called from outside a shell
buffer, find the most `appropriate shell' and switch to it. Once
switched, set default directory in that target shell to the directory
of the source buffer.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-wizards-shell-by-key "emacspeak-wizards" "\
Switch to shell buffer by key. This provides a predictable means for
  switching to a specific shell buffer. When invoked from a
  non-shell-mode buffer that is visiting a file, invokes `cd ' in the
  shell to change to the value of `default-directory' --- if called with  a
  prefix-arg. When already in a shell buffer,
  interactive prefix arg `prefix' causes this shell to be re-keyed if
  appropriate --- see \\[emacspeak-wizards-shell-re-key] for an
  explanation of how re-keying works.

\(fn &optional PREFIX)" t nil)

(autoload 'emacspeak-wizards-project-shells-initialize "emacspeak-wizards" "\
Create shells per `emacspeak-wizards-project-shells'.

\(fn)" nil nil)

(autoload 'emacspeak-wizards-shell-directory-set "emacspeak-wizards" "\
Define current directory as this shell's project directory.

\(fn)" t nil)

(autoload 'emacspeak-wizards-shell-directory-reset "emacspeak-wizards" "\
Set current directory to this shell's initial directory if one was defined.

\(fn)" t nil)

(autoload 'emacspeak-wizards-show-commentary "emacspeak-wizards" "\
Display commentary. Default is to display commentary from current buffer.

\(fn &optional FILE)" t nil)

(autoload 'emacspeak-wizards-unhex-uri "emacspeak-wizards" "\
UnEscape URI

\(fn URI)" t nil)

(autoload 'emacspeak-wizards-add-autoload-cookies "emacspeak-wizards" "\
Add autoload cookies to file f.
Default is to add autoload cookies to current file.

\(fn &optional F)" t nil)

(autoload 'emacspeak-wizards-thanks-mail-signature "emacspeak-wizards" "\
insert thanks , --Raman at the end of mail message

\(fn)" t nil)

(autoload 'emacspeak-wizards-popup-input-buffer "emacspeak-wizards" "\
Provide an input buffer in a specified mode.

\(fn MODE)" t nil)

(autoload 'emacspeak-wizards-find-emacspeak-source "emacspeak-wizards" "\
Like find-file, but binds default-directory to emacspeak-directory.

\(fn)" t nil)

(autoload 'emacspeak-wizards-next-bullet "emacspeak-wizards" "\
Navigate to and speak next `bullet'.

\(fn)" t nil)

(autoload 'emacspeak-wizards-previous-bullet "emacspeak-wizards" "\
Navigate to and speak previous `bullet'.

\(fn)" t nil)

(autoload 'emacspeak-wizards-braille "emacspeak-wizards" "\
Insert Braille string at point.

\(fn S)" t nil)

(autoload 'emacspeak-wizards-cycle-to-previous-buffer "emacspeak-wizards" "\
Cycles to previous buffer having same mode.

\(fn)" t nil)

(autoload 'emacspeak-wizards-cycle-to-next-buffer "emacspeak-wizards" "\
Cycles to next buffer having same mode.

\(fn)" t nil)

(autoload 'emacspeak-wizards-term "emacspeak-wizards" "\
Switch to an ansi-term buffer or create one.
With prefix arg, always creates a new terminal.
Otherwise cycles through existing terminals, creating the first
term if needed.

\(fn CREATE)" t nil)

(autoload 'emacspeak-wizards-espeak-string "emacspeak-wizards" "\
Speak string in lang via ESpeak.
Lang is obtained from property `lang' on string, or  via an interactive prompt.

\(fn STRING)" t nil)

(autoload 'emacspeak-wizards-espeak-region "emacspeak-wizards" "\
Speak region using ESpeak polyglot wizard.

\(fn START END)" t nil)

(autoload 'emacspeak-wizards-espeak-line "emacspeak-wizards" "\
Speak line using espeak polyglot wizard.

\(fn)" t nil)

(autoload 'emacspeak-wizards-enumerate-matching-commands "emacspeak-wizards" "\
Return list of commands whose names match pattern.

\(fn PATTERN)" t nil)

(autoload 'emacspeak-wizards-enumerate-uncovered-commands "emacspeak-wizards" "\
Enumerate unadvised commands matching pattern.
Optional interactive prefix arg `bound-only'
filters out commands that dont have an active key-binding.

\(fn PATTERN &optional BOUND-ONLY)" t nil)

(autoload 'emacspeak-wizards-enumerate-unmapped-faces "emacspeak-wizards" "\
Enumerate unmapped faces matching pattern.

\(fn &optional PATTERN)" t nil)

(autoload 'emacspeak-wizards-enumerate-obsolete-faces "emacspeak-wizards" "\
utility function to enumerate old, obsolete maps that we have still
mapped to voices.

\(fn)" t nil)

(autoload 'emacspeak-wizards-execute-emacspeak-command "emacspeak-wizards" "\
Prompt for and execute an Emacspeak command.

\(fn COMMAND)" t nil)

(autoload 'emacspeak-wizards-sunrise-sunset "emacspeak-wizards" "\
Display sunrise/sunset for specified address.

\(fn ADDRESS &optional ARG)" t nil)

(autoload 'emacspeak-wizards-shell-command-on-current-file "emacspeak-wizards" "\
Prompts for and runs shell command on current file.

\(fn COMMAND)" t nil)

(autoload 'emacspeak-wizards-view-buffers-filtered-by-mode "emacspeak-wizards" "\
Display list of buffers filtered by specified mode.

\(fn MODE)" t nil)

(autoload 'emacspeak-wizards-view-buffers-filtered-by-this-mode "emacspeak-wizards" "\
Buffer menu filtered by  mode of current-buffer.

\(fn)" t nil)

(autoload 'emacspeak-wizards-view-buffers-filtered-by-m-player-mode "emacspeak-wizards" "\
Buffer menu filtered by  m-player mode.

\(fn)" t nil)

(autoload 'emacspeak-wizards-eww-buffer-list "emacspeak-wizards" "\
Display list of open EWW buffers.

\(fn)" t nil)

(autoload 'emacspeak-wizards-tune-in-radio-browse "emacspeak-wizards" "\
Browse Tune-In Radio.
Optional interactive prefix arg `category' prompts for a category.

\(fn &optional CATEGORY)" t nil)

(autoload 'emacspeak-wizards-tune-in-radio-search "emacspeak-wizards" "\
Search Tune-In Radio.

\(fn)" t nil)

(autoload 'emacspeak-wizards-iheart "emacspeak-wizards" "\
Perform IHeart Radio search and display clickable results.

\(fn Q)" t nil)

(autoload 'emacspeak-wizards-yql-lookup "emacspeak-wizards" "\
Lookup quotes for specified stock symbols.
Symbols are separated by whitespace.

\(fn SYMBOLS)" t nil)

(autoload 'emacspeak-wizards-yql-quotes "emacspeak-wizards" "\
Display quotes using YQL API.
Symbols are taken from `emacspeak-wizards-personal-portfolio'.

\(fn)" t nil)

(defvar emacspeak-wizards-alpha-vantage-api-key nil "\
API Key  used to retrieve stock data from alpha-vantage.
Visit https://www.alphavantage.co/support/#api-key to get your key.")

(custom-autoload 'emacspeak-wizards-alpha-vantage-api-key "emacspeak-wizards" t)

(defconst ems--alpha-vantage-funcs '("TIME_SERIES_INTRADAY" "TIME_SERIES_DAILY_ADJUSTED" "TIME_SERIES_WEEKLY_ADJUSTED" "TIME_SERIES_MONTHLY_ADJUSTED") "\
Alpha-Vantage query types.")

(defvar emacspeak-wizards-iex-quotes-row-filter '(0 " ask  " 2 " trading between   " 4 " and  " 5 " PE is " 12 " for a market cap of " 11 "the 52 week average is " 9 "and the 200 day moving average is " 10) "\
Template used to audio-format  rows.")

(custom-autoload 'emacspeak-wizards-iex-quotes-row-filter "emacspeak-wizards" t)

(autoload 'emacspeak-wizards-iex-show-price "emacspeak-wizards" "\
Quick Quote: Just stock price from IEX Trading.

\(fn SYMBOL)" t nil)

(autoload 'emacspeak-wizards-iex-show-quote "emacspeak-wizards" "\
Show portfolio  data from cache.
Optional interactive prefix arg forces cache refresh.

The quotes view uses emacspeak's table mode.
In addition,  the following  keys are available :

F: Show financials for current stock.
N: Show news for current stock.
P: Show live price for current stock.

\(fn &optional REFRESH)" t nil)

(autoload 'emacspeak-wizards-iex-show-news "emacspeak-wizards" "\
Show news for specified ticker.
Checks cache, then makes API call if needed.
Optional interactive prefix arg refreshes cache.

\(fn SYMBOL &optional REFRESH)" t nil)

(autoload 'emacspeak-wizards-iex-show-financials "emacspeak-wizards" "\
Show financials for specified ticker.
Checks cache, then makes API call if needed.
Optional interactive prefix arg refreshes cache.

\(fn SYMBOL &optional REFRESH)" t nil)

(autoload 'emacspeak-wizards-quote "emacspeak-wizards" "\
Top-level dispatch for looking up Stock Market information.

Key:Action
f: Financials
G: finance Google Search
n: News
p: Price
q: Quotes

\(fn &optional REFRESH)" t nil)

(autoload 'emacspeak-wizards-set-colors "emacspeak-wizards" "\
Interactively prompt for foreground and background colors.

\(fn)" t nil)

(autoload 'emacspeak-wizards-color-diff-at-point "emacspeak-wizards" "\
Meaningfully speak difference between background and foreground color at point.
With interactive prefix arg, set foreground and background color first.

\(fn &optional SET)" t nil)

(autoload 'emacspeak-wizards-colors "emacspeak-wizards" "\
Display list of colors and setup a callback to activate color
under point as either the foreground or background color.

\(fn)" t nil)

(autoload 'emacspeak-wizards-color-at-point "emacspeak-wizards" "\
Echo foreground/background color at point.

\(fn)" t nil)

(autoload 'emacspeak-wizards-swap-fg-and-bg "emacspeak-wizards" "\
Swap foreground and background.

\(fn)" t nil)

(autoload 'emacspeak-wizards-pipe "emacspeak-wizards" "\
convenience function

\(fn)" nil nil)

(autoload 'emacspeak-wizards-scratch "emacspeak-wizards" "\
Switch to *scratch* buffer, creating it if necessary.

\(fn)" t nil)

(autoload 'emacspeak-wizards-quick-weather "emacspeak-wizards" "\
Bring up weather forecast for current location.

\(fn)" t nil)

(autoload 'emacspeak-wizards-noaa-weather "emacspeak-wizards" "\
Display weather information using NOAA Weather API.
Data is retrieved only once, subsequent calls switch to previously
displayed results. Kill that buffer or use an interactive prefix
arg (C-u) to get new data.  Optional second interactive prefix
arg (C-u C-u) asks for location address; Default is to display
weather for `gweb-my-address'.  

\(fn &optional ASK)" t nil)

(autoload 'emacspeak-wizards-bash-completion-toggle "emacspeak-wizards" "\
Toggle bash completion from package bash-completion in this shell.

\(fn)" t nil)

(autoload 'emacspeak-wizards-wc-2018 "emacspeak-wizards" "\
Display Soccer World Cup Card From Google.

\(fn TEAM)" t nil)

(autoload 'emacspeak-wizards-google-news "emacspeak-wizards" "\
Clean up news.google.com for  skimming the news.

\(fn)" t nil)

(autoload 'emacspeak-wizards-google-headlines "emacspeak-wizards" "\
Display just the headlines from Google News for fast loading.

\(fn)" t nil)

(autoload 'emacspeak-wizards-execute-asynchronously "emacspeak-wizards" "\
Read key-sequence, then execute its command on a new thread.

\(fn KEY)" t nil)

(autoload 'emacspeak-wizards-insert-elisp-prefix "emacspeak-wizards" "\
Insert package prefix for current file.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-wizards" '("emacspeak-" "ems-" "yql-")))

;;;***

;;;### (autoloads nil "emacspeak-xkcd" "emacspeak-xkcd.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-xkcd.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-xkcd" '("emacspeak-xkcd-" "xkcd-transcript")))

;;;***

;;;### (autoloads nil "emacspeak-xml-shell" "emacspeak-xml-shell.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from emacspeak-xml-shell.el

(let ((loads (get 'emacspeak-xml-shell 'custom-loads))) (if (member '"emacspeak-xml-shell" loads) nil (put 'emacspeak-xml-shell 'custom-loads (cons '"emacspeak-xml-shell" loads))))

(autoload 'emacspeak-xml-shell "emacspeak-xml-shell" "\
Start Xml-Shell on contents of system-id.

\(fn SYSTEM-ID)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-xml-shell" '("emacspeak-xml-shell-")))

;;;***

;;;### (autoloads nil "emacspeak-xslt" "emacspeak-xslt.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from emacspeak-xslt.el

(autoload 'emacspeak-xslt-get "emacspeak-xslt" "\
Return fully qualified stylesheet path.

\(fn STYLE)" nil nil)

(defvar emacspeak-xslt-options "--html --nonet --novalid --encoding utf-8" "\
Options passed to xsltproc.")

(custom-autoload 'emacspeak-xslt-options "emacspeak-xslt" t)

(autoload 'emacspeak-xslt-region "emacspeak-xslt" "\
Apply XSLT transformation to region and replace it with
the result.  This uses XSLT processor xsltproc available as
part of the libxslt package.

\(fn XSL START END &optional PARAMS NO-COMMENT)" nil nil)

(autoload 'emacspeak-xslt-run "emacspeak-xslt" "\
Run xslt on region, and return output filtered by sort -u.
Region defaults to entire buffer.

\(fn XSL &optional START END)" nil nil)

(autoload 'emacspeak-xslt-url "emacspeak-xslt" "\
Apply XSLT transformation to url
and return the results in a newly created buffer.
  This uses XSLT processor xsltproc available as
part of the libxslt package.

\(fn XSL URL &optional PARAMS NO-COMMENT)" nil nil)

(autoload 'emacspeak-xslt-xml-url "emacspeak-xslt" "\
Apply XSLT transformation to XML url
and return the results in a newly created buffer.
  This uses XSLT processor xsltproc available as
part of the libxslt package.

\(fn XSL URL &optional PARAMS)" nil nil)

(autoload 'emacspeak-xslt-view-file "emacspeak-xslt" "\
Transform `file' using `style' and preview via browse-url.

\(fn STYLE FILE)" t nil)

(autoload 'emacspeak-xslt-view "emacspeak-xslt" "\
Browse URL with specified XSL style.

\(fn STYLE URL)" t nil)

(autoload 'emacspeak-xslt-view-xml "emacspeak-xslt" "\
Browse XML URL with specified XSL style.

\(fn STYLE URL &optional UNESCAPE-CHARENT)" t nil)

(autoload 'emacspeak-xslt-view-region "emacspeak-xslt" "\
Browse XML region with specified XSL style.

\(fn STYLE START END &optional UNESCAPE-CHARENT)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "emacspeak-xslt" '("emacspeak-xslt-")))

;;;***

;;;### (autoloads nil "espeak-voices" "espeak-voices.el" (0 0 0 0))
;;; Generated autoloads from espeak-voices.el

(defvar espeak-default-speech-rate 175 "\
Default speech rate for eSpeak.")

(custom-autoload 'espeak-default-speech-rate "espeak-voices" nil)

(autoload 'espeak "espeak-voices" "\
Start ESpeak engine.

\(fn)" t nil)

(autoload 'espeak-configure-tts "espeak-voices" "\
Configure TTS environment to use eSpeak.

\(fn)" nil nil)

(autoload 'espeak-make-tts-env "espeak-voices" "\
Constructs a TTS environment for Espeak.

\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "espeak-voices" '("espeak-" "tts-default-voice")))

;;;***

;;;### (autoloads nil "ladspa" "ladspa.el" (0 0 0 0))
;;; Generated autoloads from ladspa.el

(defconst ladspa-home (or (getenv "LADSPA_PATH") "/usr/lib/ladspa") "\
Instalation location for Ladspa plugins.")

(autoload 'ladspa "ladspa" "\
Launch Ladspa workbench.

\(fn &optional REFRESH)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "ladspa" '("ladspa-")))

;;;***

;;;### (autoloads nil "mac-voices" "mac-voices.el" (0 0 0 0))
;;; Generated autoloads from mac-voices.el

(defvar mac-default-speech-rate 225 "\
Default speech rate for mac.")

(custom-autoload 'mac-default-speech-rate "mac-voices" nil)

(autoload 'mac-configure-tts "mac-voices" "\
Configure TTS environment to use mac  family of synthesizers.

\(fn)" nil nil)

(autoload 'mac-make-tts-env "mac-voices" "\
Constructs a TTS environment for Mac.

\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "mac-voices" '("mac-")))

;;;***

;;;### (autoloads nil "nm" "nm.el" (0 0 0 0))
;;; Generated autoloads from nm.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "nm" '("nm-")))

;;;***

;;;### (autoloads nil "outloud-voices" "outloud-voices.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from outloud-voices.el

(defvar outloud-default-speech-rate 50 "\
Default speech rate for outloud.")

(custom-autoload 'outloud-default-speech-rate "outloud-voices" nil)

(autoload 'outloud-configure-tts "outloud-voices" "\
Configure TTS environment to use ViaVoice  family of synthesizers.

\(fn)" nil nil)

(autoload 'outloud-make-tts-env "outloud-voices" "\
Constructs a TTS environment for Outloud.

\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "outloud-voices" '("outloud")))

;;;***

;;;### (autoloads nil "plain-voices" "plain-voices.el" (0 0 0 0))
;;; Generated autoloads from plain-voices.el

(autoload 'plain-configure-tts "plain-voices" "\
Configures TTS environment to use Plain family of synthesizers.

\(fn)" nil nil)

(autoload 'plain-make-tts-env "plain-voices" "\
Constructs a TTS environment for Plain.

\(fn)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "plain-voices" '("plain")))

;;;***

;;;### (autoloads nil "soundscape" "soundscape.el" (0 0 0 0))
;;; Generated autoloads from soundscape.el

(defvar soundscape-device nil "\
Alsa sound device to use for soundscapes.")

(custom-autoload 'soundscape-device "soundscape" t)

(defvar soundscape-manager-options '("-o" "alsa" "-m" "0.3") "\
User customizable options list passed to boodler.
Defaults specify alsa as the output and set master volume to 0.5")

(custom-autoload 'soundscape-manager-options "soundscape" t)

(autoload 'soundscape "soundscape" "\
Play soundscape.

\(fn SCAPE)" t nil)

(autoload 'soundscape-load-theme "soundscape" "\
Sets up automatic Soundscape mappings based on theme.
See  \\{soundscape-default-theme} for details.

\(fn THEME)" nil nil)

(autoload 'soundscape-init "soundscape" "\
Initialize Soundscape module.

\(fn)" nil nil)

(autoload 'soundscape-listener "soundscape" "\
Start  a Soundscape listener.
Listener is loaded with all Soundscapes defined in `soundscape-default-theme' .
Optional interactive prefix arg restarts the listener if already running.

\(fn &optional RESTART)" t nil)

(defvar soundscape--auto nil "\
Record if automatic soundscapes are on.
Do not set this by hand, use command \\[soundscape-toggle].")

(defvar soundscape-idle-delay 0.1 "\
Number of seconds of idle time
before soundscapes are synchronized with current mode.")

(custom-autoload 'soundscape-idle-delay "soundscape" t)

(autoload 'soundscape-toggle "soundscape" "\
Toggle automatic SoundScapes.
When turned on, Soundscapes are automatically run based on current major mode.
Run command \\[soundscape-theme] to see the default mode->mood mapping.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "soundscape" '("soundscape-")))

;;;***

;;;### (autoloads nil "sox" "sox.el" (0 0 0 0))
;;; Generated autoloads from sox.el

(autoload 'sox "sox" "\
Create a new Audio Workbench or switch to an existing workbench.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "sox" '("sox-")))

;;;***

;;;### (autoloads nil "sox-gen" "sox-gen.el" (0 0 0 0))
;;; Generated autoloads from sox-gen.el

(defvar sox-gen-p (executable-find "sox") "\
Should sox-gen commands attempt to invoke SoX.")

(custom-autoload 'sox-gen-p "sox-gen" t)

(autoload 'sox-tone-binaural "sox-gen" "\
Play binaural audio with carrier frequency `freq', beat `beat', and
gain `gain'.

\(fn LENGTH FREQ BEAT GAIN)" t nil)

(autoload 'sox-tone-slide-binaural "sox-gen" "\
Play binaural audio with carrier frequency `freq', beat
`beat-start' -> `beat-end', and gain `gain'.

\(fn LENGTH FREQ BEAT-START BEAT-END GAIN)" t nil)

(autoload 'sox-beats-binaural "sox-gen" "\
Play binaural audio with beat-spec specifying the various tones.
Param `beat-spec-list' is a list of `(carrier beat) tupples.

\(fn LENGTH BEAT-SPEC-LIST GAIN)" t nil)

(autoload 'sox-binaural "sox-gen" "\
Play specified binaural effect.

\(fn NAME DURATION)" t nil)

(autoload 'sox-slide-binaural "sox-gen" "\
Play specified binaural slide from `name-1' to `name-2'.

\(fn NAME-1 NAME-2 DURATION)" t nil)

(autoload 'sox-rev-up "sox-gen" "\
Play rev-up set of  binaural beats.
Param `length' specifies total duration.

\(fn LENGTH)" t nil)

(autoload 'sox-turn-down "sox-gen" "\
Play turn-down set of  binaural beats.
Param `length' specifies total duration.

\(fn LENGTH)" t nil)

(autoload 'sox-wind-down "sox-gen" "\
Play wind-down set of  binaural beats.
Param `length' specifies total duration.

\(fn LENGTH)" t nil)

(autoload 'sox-relax "sox-gen" "\
Play relax set of  binaural beats.
Param `length' specifies total duration.

\(fn LENGTH)" t nil)

(autoload 'sox-chakras "sox-gen" "\
Play each chakra for specified duration.
Parameter `theme' specifies variant.

\(fn THEME DURATION)" t nil)

(autoload 'sox-chime "sox-gen" "\
Play chime --- optional args tempo and speed default to 1.

\(fn &optional TEMPO SPEED)" nil nil)

(autoload 'sox-multiwindow "sox-gen" "\
Produce a short note used to cue multiwindow.

\(fn &optional SWAP SPEED)" nil nil)

(autoload 'sox-do-scroll-up "sox-gen" "\
Produce a short do-scroll-up.

\(fn &optional SPEED)" nil nil)

(autoload 'sox-do-scroll-down "sox-gen" "\
Produce a short do-scroll-down.

\(fn &optional SPEED)" nil nil)

(autoload 'sox-tones "sox-gen" "\
Play sequence of tones --- optional args tempo and speed default to 1.

\(fn &optional TEMPO SPEED)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "sox-gen" '("sox-")))

;;;***

;;;### (autoloads nil "stack-f" "stack-f.el" (0 0 0 0))
;;; Generated autoloads from stack-f.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "stack-f" '("stack-")))

;;;***

;;;### (autoloads nil "tapestry" "tapestry.el" (0 0 0 0))
;;; Generated autoloads from tapestry.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "tapestry" '("set-tapestry" "tapestry")))

;;;***

;;;### (autoloads nil "tetris" "tetris.el" (0 0 0 0))
;;; Generated autoloads from tetris.el

(autoload 'tetris "tetris" "\
Play the Tetris game.
Shapes drop from the top of the screen, and the user has to move and
rotate the shape to fit in with those at the bottom of the screen so
as to form complete rows.

tetris-mode keybindings:
   \\<tetris-mode-map>
\\[tetris-start-game]   Starts a new game of Tetris
\\[tetris-end-game]     Terminates the current game
\\[tetris-pause-game]   Pauses (or resumes) the current game
\\[tetris-move-left]    Moves the shape one square to the left
\\[tetris-move-right]   Moves the shape one square to the right
\\[tetris-rotate-prev]  Rotates the shape clockwise
\\[tetris-rotate-next]  Rotates the shape anticlockwise
\\[tetris-move-bottom]  Drops the shape to the bottom of the playing area

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "tetris" '("tetris-")))

;;;***

;;;### (autoloads nil "toy-braille" "toy-braille.el" (0 0 0 0))
;;; Generated autoloads from toy-braille.el

(autoload 'get-toy-braille-string "toy-braille" "\


\(fn INSTR)" nil nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "toy-braille" '("toy-braille-map")))

;;;***

;;;### (autoloads nil "tts" "tts.el" (0 0 0 0))
;;; Generated autoloads from tts.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "tts" '("tts-")))

;;;***

;;;### (autoloads nil "tts-cmds" "tts-cmds.el" (0 0 0 0))
;;; Generated autoloads from tts-cmds.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "tts-cmds" '("tts-cmd-set-rate")))

;;;***

;;;### (autoloads nil "voice-setup" "voice-setup.el" (0 0 0 0))
;;; Generated autoloads from voice-setup.el

(autoload 'voice-lock-mode "voice-setup" "\
Toggle voice lock mode.

If called interactively, enable Voice-Lock mode if ARG is positive, and
disable it if ARG is zero or negative.  If called from Lisp,
also enable the mode if ARG is omitted or nil, and toggle it
if ARG is `toggle'; disable the mode otherwise.

\(fn &optional ARG)" t nil)

(autoload 'turn-on-voice-lock "voice-setup" "\
Turn on Voice Lock mode .

\(fn)" t nil)

(autoload 'turn-off-voice-lock "voice-setup" "\
Turn off Voice Lock mode .

\(fn)" t nil)

(defvar global-voice-lock-mode t "\
Global value of voice-lock-mode.")

(autoload 'voice-setup-toggle-silence-personality "voice-setup" "\
Toggle audibility of personality under point  .
If personality at point is currently audible, its
face->personality map is cached in a buffer local variable, and
its face->personality map is replaced by face->inaudible.  If
personality at point is inaudible, and there is a cached value,
then the original face->personality mapping is restored.  In
either case, the buffer is refontified to have the new mapping
take effect.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "voice-setup" '("defvoice" "global-voice-lock-mode" "voice-")))

;;;***

;;;### (autoloads nil "xbacklight" "xbacklight.el" (0 0 0 0))
;;; Generated autoloads from xbacklight.el

(autoload 'xbacklight-get "xbacklight" "\
Get current brightness level.

\(fn)" t nil)

(autoload 'xbacklight-set "xbacklight" "\
Set brightness to  specified level.
`brightness' is a percentage value.

\(fn BRIGHTNESS)" t nil)

(autoload 'xbacklight-increment "xbacklight" "\
Increase brightness by  by one step.

\(fn)" t nil)

(autoload 'xbacklight-decrement "xbacklight" "\
Decrease brightness by  by one step.

\(fn)" t nil)

(autoload 'xbacklight-black "xbacklight" "\
Turn screen black.

\(fn)" t nil)

(autoload 'xbacklight-white "xbacklight" "\
Turn screen white.

\(fn)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "xbacklight" '("xbacklight-")))

;;;***

;;;### (autoloads nil nil ("emacspeak-add-log.el" "emacspeak-apt-sources.el"
;;;;;;  "emacspeak-auctex.el" "emacspeak-bbdb.el" "emacspeak-bibtex.el"
;;;;;;  "emacspeak-bookmark.el" "emacspeak-browse-kill-ring.el" "emacspeak-calc.el"
;;;;;;  "emacspeak-cedet.el" "emacspeak-checkdoc.el" "emacspeak-cider.el"
;;;;;;  "emacspeak-ciel.el" "emacspeak-clojure.el" "emacspeak-cmuscheme.el"
;;;;;;  "emacspeak-desktop.el" "emacspeak-dictionary.el" "emacspeak-diff-mode.el"
;;;;;;  "emacspeak-eclim.el" "emacspeak-elisp-refs.el" "emacspeak-elpy.el"
;;;;;;  "emacspeak-elscreen.el" "emacspeak-epa.el" "emacspeak-ess.el"
;;;;;;  "emacspeak-facemenu.el" "emacspeak-find-dired.el" "emacspeak-flycheck.el"
;;;;;;  "emacspeak-folding.el" "emacspeak-ftf.el" "emacspeak-geiser.el"
;;;;;;  "emacspeak-generic.el" "emacspeak-gnuplot.el" "emacspeak-go-mode.el"
;;;;;;  "emacspeak-gtags.el" "emacspeak-gud.el" "emacspeak-hideshow.el"
;;;;;;  "emacspeak-indium.el" "emacspeak-jdee.el" "emacspeak-jss.el"
;;;;;;  "emacspeak-kite.el" "emacspeak-kmacro.el" "emacspeak-lua.el"
;;;;;;  "emacspeak-make-mode.el" "emacspeak-markdown.el" "emacspeak-metapost.el"
;;;;;;  "emacspeak-mspools.el" "emacspeak-muggles-autoloads.el" "emacspeak-muse.el"
;;;;;;  "emacspeak-perl.el" "emacspeak-php-mode.el" "emacspeak-py.el"
;;;;;;  "emacspeak-pydoc.el" "emacspeak-python.el" "emacspeak-racket.el"
;;;;;;  "emacspeak-re-builder.el" "emacspeak-reftex.el" "emacspeak-related.el"
;;;;;;  "emacspeak-rg.el" "emacspeak-rpm.el" "emacspeak-rst.el" "emacspeak-ruby.el"
;;;;;;  "emacspeak-sgml-mode.el" "emacspeak-sh-script.el" "emacspeak-sigbegone.el"
;;;;;;  "emacspeak-slime.el" "emacspeak-smart-window.el" "emacspeak-smartparens.el"
;;;;;;  "emacspeak-sql.el" "emacspeak-supercite.el" "emacspeak-tdtd.el"
;;;;;;  "emacspeak-tempo.el" "emacspeak-tide.el" "emacspeak-typo.el"
;;;;;;  "emacspeak-wdired.el" "emacspeak-windmove.el" "emacspeak-winring.el"
;;;;;;  "emacspeak-woman.el" "emacspeak-xref.el" "emacspeak-yaml.el"
;;;;;;  "emacspeak-yasnippet.el") (0 0 0 0))

;;;***

;;;### (autoloads nil "acss-structure" "acss-structure.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from acss-structure.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "acss-structure" '("acss-personality-from-speech-style" "tts-default-voice")))

;;;***
